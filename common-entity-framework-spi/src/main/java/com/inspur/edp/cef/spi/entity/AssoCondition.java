/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity;

public class AssoCondition {
    private String leftNodeCode;
    //前期设计器没有出来，暂时不赋值
    private String leftElemenetId;
    private String leftField;

    public String getLeftNodeCode() {
        return leftNodeCode;
    }

    public void setLeftNodeCode(String leftNodeCode) {
        this.leftNodeCode = leftNodeCode;
    }

    public String getLeftField() {
        return leftField;
    }

    public void setLeftField(String leftField) {
        this.leftField = leftField;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getRightNodeCode() {
        return rightNodeCode;
    }

    public void setRightNodeCode(String rightNodeCode) {
        this.rightNodeCode = rightNodeCode;
    }

    public String getRightField() {
        return rightField;
    }

    public void setRightField(String rightField) {
        this.rightField = rightField;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public String getLeftElemenetId() {
        return leftElemenetId;
    }

    public void setLeftElemenetId(String leftElemenetId) {
        this.leftElemenetId = leftElemenetId;
    }

    public String getRightElementId() {
        return rightElementId;
    }

    public void setRightElementId(String rightElementId) {
        this.rightElementId = rightElementId;
    }
    private String operator;
    private String rightNodeCode;
    private String rightElementId;
    private String rightField;
    private String value;
}
