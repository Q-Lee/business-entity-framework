/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.info.propertyinfo;

import com.fasterxml.jackson.databind.JsonNode;
import com.inspur.edp.cef.api.manager.serialize.CefSerializeContext;
import com.inspur.edp.cef.entity.entity.FieldType;
import com.inspur.edp.cef.spi.jsonser.util.SerializerUtil;

import java.util.HashMap;
import java.util.Map;

public class SimpleUdtPropertyInfo extends UdtPropertyInfo {

    private final FieldType fieldType;

    public SimpleUdtPropertyInfo(String udtConfigId, boolean enableStdTimeFormat) {
        this(udtConfigId,enableStdTimeFormat,FieldType.String);
    }

    public SimpleUdtPropertyInfo(String udtConfigId, boolean enableStdTimeFormat, FieldType fieldType) {
        super(udtConfigId, enableStdTimeFormat);
        this.fieldType = fieldType;
    }

    private HashMap<String,String> propMapping;


    @Override
    public Object read(JsonNode node, String propertyName, CefSerializeContext serContext) {

        HashMap<String, JsonNode> nodes = new HashMap();
        nodes.put(propertyName, node);
        context.setJsonFormatType(serContext.getJsonFormatType());
        return SerializerUtil.readNestedValue(udtConfigId, nodes, null, context);
    }

    public FieldType getFieldType() {
        return fieldType;
    }

    public HashMap<String, String> getPropMapping() {
        return propMapping;
    }

    public void setPropMapping(HashMap<String, String> propMapping) {
        this.propMapping = propMapping;
    }

//    @Override
//    public Object read(JsonNode node, String propertyName, CefSerializeContext serContext) {
//        IUdtData data = (IUdtData) createValue();
//
//        return ;
//    }


}
