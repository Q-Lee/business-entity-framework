/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.validation.basetypes.nestedtrans;

import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObject;
import com.inspur.edp.cef.api.manager.ICefValueObjManager;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.cef.spi.common.UdtManagerUtil;
import com.inspur.edp.cef.spi.validation.IValidation;

public abstract class CefNestedTransValAdapter implements IValidation {

  private final String name;
  protected final String propertyName;
  protected final String udtConfig;

  public CefNestedTransValAdapter(String name,String propertyName,String udtConfig)
  {
    this.name = name;
    this.propertyName = propertyName;
    this.udtConfig = udtConfig;
  }
  @Override
  public abstract boolean canExecute(IChangeDetail change);


  @Override
  public void execute(ICefValidationContext context, IChangeDetail change) {
    if (context.getData() == null)     return;
    IValueObjData data= (IValueObjData) context.getData().getValue(propertyName);
    if (data == null)     return;
    ICefValueObjManager valueObjMgr=(ICefValueObjManager) UdtManagerUtil.getUdtFactory().createManager(udtConfig);
    ICefValueObject valueObj=valueObjMgr.createValueObject(data);
    doExecute(valueObj, context, change);
  }

  protected abstract void doExecute(ICefValueObject valueObject,ICefValidationContext context, IChangeDetail change);
}
