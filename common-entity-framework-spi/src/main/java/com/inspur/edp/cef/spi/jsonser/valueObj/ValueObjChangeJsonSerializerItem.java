/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.jsonser.valueobj;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefValueObjResInfo;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataSerItem;

public class ValueObjChangeJsonSerializerItem extends AbstractCefDataSerItem {

  private CefValueObjResInfo valueObjResInfo;

  public ValueObjChangeJsonSerializerItem(CefValueObjResInfo valueObjResInfo)
  {
    this.valueObjResInfo = valueObjResInfo;
  }
  @Override
  public void writeEntityBasicInfo(JsonGenerator jsonGenerator, ICefData data,
      SerializerProvider serializerProvider) {

  }

  @Override
  public boolean writeModifyPropertyJson(JsonGenerator jsonGenerator, String propertyName,
      Object value, SerializerProvider serializerProvider) {
    return false;
  }

  @Override
  public boolean readEntityBasicInfo(JsonParser p, DeserializationContext ctxt, ICefData data,
      String propertyName) {
    return false;
  }
}
