/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.jsonser.abstractcefchange;

import com.fasterxml.jackson.core.JsonGenerator;
import com.inspur.edp.cef.entity.entity.IMultiLanguageData;
import com.inspur.edp.cef.entity.i18n.MultiLanguageInfo;
import com.inspur.edp.cef.spi.jsonser.base.StringUtils;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

public class MultiLangSerializationUtil {

  private MultiLangSerializationUtil() {
  }

  public static void serialize(JsonGenerator jsonGenerator,
      IMultiLanguageData multiLanguageData) throws IOException {
    Map<String, MultiLanguageInfo> infos = multiLanguageData.getMultiLanguageInfos();
    if (infos == null) {
      return;
    }
    for (Entry<String, MultiLanguageInfo> entrySet : infos.entrySet()) {
      String propName = entrySet.getKey().split(MultiLanguageInfo.MULTILANGUAGETOKEN)[0];
      // 此处2步处理，为了只对属性名toCamel，不影响后缀
      jsonGenerator.writeFieldName(
          StringUtils.toCamelCase(propName).concat(MultiLanguageInfo.MULTILANGUAGETOKEN));
      if (entrySet.getValue() == null) {
        jsonGenerator.writeObject(null);
        continue;
      }
      jsonGenerator.writeObject(entrySet.getValue().getPropValueMap());
    }
  }
}
