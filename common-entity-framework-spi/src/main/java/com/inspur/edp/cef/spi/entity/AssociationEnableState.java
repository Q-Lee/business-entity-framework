/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity;

public enum AssociationEnableState
{
	Enabled(0),
	Disabled(1);

	private int intValue;
	private static java.util.HashMap<Integer, AssociationEnableState> mappings;
	private synchronized static java.util.HashMap<Integer, AssociationEnableState> getMappings()
	{
		if (mappings == null)
		{
			mappings = new java.util.HashMap<Integer, AssociationEnableState>();
		}
		return mappings;
	}

	private AssociationEnableState(int value)
	{
		intValue = value;
		AssociationEnableState.getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static AssociationEnableState forValue(int value)
	{
		return getMappings().get(value);
	}
}
