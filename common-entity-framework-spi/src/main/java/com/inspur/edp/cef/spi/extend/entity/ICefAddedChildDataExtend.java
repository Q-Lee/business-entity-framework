/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.extend.entity;

import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;

public interface ICefAddedChildDataExtend {

  String getCode();

  ICefData createData();

  IAccessor createAccessor(ICefData data);

  IAccessor createReadonlyAccessor(ICefData data);

  IEntityDataCollection createDataCollection();

  IEntityDataCollection createAccessorCollection(IEntityData parent);

  IEntityDataCollection createReadonlyAccessorCollection(IEntityData parent);
}
