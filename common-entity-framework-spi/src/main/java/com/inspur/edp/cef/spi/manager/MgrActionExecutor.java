/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.manager;

import com.inspur.edp.cef.spi.mgraction.CefMgrAction;
import com.inspur.edp.cef.api.manager.ICefManagerContext;
import com.inspur.edp.cef.api.manager.ICefMgrActionAssembler;
import com.inspur.edp.cef.api.manager.action.ICefMgrAction;
import com.inspur.edp.cef.api.manager.action.IMgrActionExecutor;

//TODO:挪到core里
public class MgrActionExecutor<T> implements IMgrActionExecutor<T>
{
	private ICefManagerContext privateContext;
	public final ICefManagerContext getContext()
	{
		return privateContext;
	}
	public final void setContext(ICefManagerContext value)
	{
		privateContext = value;
	}

	private CefMgrAction<T> privateAction;
	public final ICefMgrAction<T> getAction()
	{
		return privateAction;
	}
	public final void setAction(ICefMgrAction<T> value)
	{
		privateAction =(CefMgrAction<T>) value;
	}

	private ICefMgrActionAssembler privateAssembler;
	protected final ICefMgrActionAssembler getAssembler()
	{
		return privateAssembler;
	}
	protected final void setAssembler(ICefMgrActionAssembler value)
	{
		privateAssembler = value;
	}

	public final T execute()
	{
		getAction().setContext(getContext());
		setAssembler(((CefMgrAction)getAction() ).innerGetActionAssembler());

		try
		{
			onBeforeExecute();

			((CefMgrAction)getAction() ).internalExecute();

			onAfterExecute();
		}
		catch (RuntimeException e)
		{
			if (!onHandleException(e))
			{
				throw e;
			}
			return null;
		}
		finally
		{
			onFinally();
		}
		return getAction().getResult();
	}

	protected void onFinally()
	{
	}

	protected boolean onHandleException(RuntimeException e)
	{
		if (getAssembler() == null)
		{
			return false;
		}

		return getAssembler().handleException(e);
	}

	protected void onBeforeExecute()
	{
		if (getAssembler() == null)
		{
			return;
		}

		getAssembler().beforeExecute();
	}

	protected void onAfterExecute()
	{
		if (getAssembler() == null)
		{
			return;
		}

		getAssembler().afterExecute();
	}
}
