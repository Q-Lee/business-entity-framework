/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.entity.info;

import java.util.HashMap;
import java.util.Map;

public final class CefEntityTypeInfo extends CefDataTypeInfo {

  private final String entityCode;

  public CefEntityTypeInfo(String entityCode,String displayValueKey) {
    super(displayValueKey);
    this.entityCode = entityCode;
  }

  private Map<String,UniqueConstraintInfo> uniqueConstraintInfos=new HashMap<>();

  public Map<String, UniqueConstraintInfo> getUniqueConstraintInfos() {
    return uniqueConstraintInfos;
  }

  public void addUnitConstraintInfo(String constraintKey,UniqueConstraintInfo uniqueConstraintInfo)
  {uniqueConstraintInfos.put(constraintKey,uniqueConstraintInfo);
  }

  public final String getEntityCode() {
    return entityCode;
  }
}
