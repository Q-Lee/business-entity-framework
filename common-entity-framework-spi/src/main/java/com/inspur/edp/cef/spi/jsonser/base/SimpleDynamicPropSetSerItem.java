/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.spi.jsonser.base;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.inspur.edp.cdf.component.api.annotation.GspComponent;

/**
 * 简单动态属性集合序列化构件, 支持仅包含string/double/long/DateTime/bool的动态属性集合.
 * <see cref="AbstractDynamicPropSetSerItem"/>
 */
@GspComponent(value = "f477939b-a944-431a-8970-bcca88f47662", id = "f477939b-a944-431a-8970-bcca88f47662")
public final class SimpleDynamicPropSetSerItem extends AbstractDynamicPropSetSerItem {
  @Override
  public Object customReadItemChange(
      String actualName, JsonParser reader, DeserializationContext serializer) {
    throw new RuntimeException();
  }

  @Override
  public Object customReadItemValue(
      String actualName, JsonParser reader, DeserializationContext serializer) {
    throw new RuntimeException();
  }

  @Override
  public void customWriteItemChange(
      String name, Object value, JsonGenerator writer, SerializerProvider serializer) {
    throw new RuntimeException();
  }

  @Override
  public void customWriteItemValue(
      String name, Object value, JsonGenerator writer, SerializerProvider serializer) {
    throw new RuntimeException();
  }

  // C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by
  // C# to Java Converter:
  @Override
  public boolean isCustomSerialize(String propName) {
    return false;
  }
}
