/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.spi;

import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataSerItem;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntityChangeSerializer;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCMEntityChangeJsonSerializer extends AbstractEntityChangeSerializer {
  public AbstractCMEntityChangeJsonSerializer(
      String nodeCode, boolean isRoot, List<AbstractEntitySerializerItem> serializers) {
    super(nodeCode, isRoot, serializers);
  }
}
