/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.spi;


import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataSerItem;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntityChangeJsonDeSerializer;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;
import com.inspur.edp.commonmodel.api.ICMManager;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCMEntityChangeJsonDeSerializer extends
		AbstractEntityChangeJsonDeSerializer
{
	public AbstractCMEntityChangeJsonDeSerializer(String nodeCode, boolean isRoot, List<AbstractEntitySerializerItem> serializers)
	{
		super(nodeCode, isRoot, serializers);
	}

//	private  static List<AbstractEntitySerializerItem> getEntitySeriaItems(List<AbstractEntitySerializerItem> serializers)
//	{
//		ArrayList<AbstractEntitySerializerItem> list =new ArrayList<>();
//		if(serializers==null)
//			return list;
//		for (AbstractEntitySerializerItem item:serializers)
//		{
//			list.add(item);
//		}
//		return list;
//	}

	@Override
	protected final IEntityData createData()
	{
		if (isRoot)
		{
			return getCMManager().createUnlistenableData("");
		}
		return getCMManager().createUnlistenableChildData(nodeCode, "");
	}

	protected abstract ICMManager getCMManager();
}
