/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.clear;

import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.exceptions.BefException;
import com.inspur.edp.bef.api.exceptions.ErrorCodes;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.bef.spi.action.AbstractAction;

public class ClearLockAction extends AbstractAction<VoidActionResult> {

  // region Constructor
  public ClearLockAction(IBEContext beContext) {
    super(beContext);
  }

  // endregion

  @Override
  public void execute() {
    if (!ActionUtil.getBEContext(this).isLocked()) {
      return;
    }
    if (ActionUtil.getBEContext(this).hasChange()) {
      throw new BefException(
          ErrorCodes.UnlockingChanged, "存在变更的数据不能解锁", null, ExceptionLevel.Error);
    }
    ReleaseLockScopeNodeParameter para =
        new ReleaseLockScopeNodeParameter(ActionUtil.getBEContext(this), getBEContext().getID());
    getBEContext().addScopeParameter(para);
  }

  @Override
  protected IBEActionAssembler getAssembler() {
    return GetAssemblerFactory().getDeleteActionAssembler(ActionUtil.getBEContext(this));
  }
}
