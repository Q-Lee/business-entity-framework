/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination;


import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

public abstract class AbstractB4QueryDtmAssembler implements IEntityRTDtmAssembler
{
	@Override
	public java.util.List<IDetermination> getBelongingDeterminations() {return null;}

	@Override
	public IChangeDetail getChangeset(ICefDataTypeContext dataTypeContext) {return null;}

	@Override
	public java.util.List<IDetermination> getChildAssemblers()  {return null;}

	@Override
	public ICefDeterminationContext getDeterminationContext(ICefEntityContext dataTypeContext) {return null;}

	@Override
	public abstract java.util.List<IDetermination> getDeterminations();
}
