/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.commonstructure;

import com.inspur.edp.bef.api.lcp.ILcpFactory;
import com.inspur.edp.bef.core.LcpUtil;
import com.inspur.edp.caf.cef.rt.api.CommonData;
import com.inspur.edp.caf.cef.rt.spi.ObjectCreateExtension;
import io.iec.edp.caf.runtime.config.CefBeanUtil;
import org.springframework.stereotype.Component;

public class BECommonDataExtension implements ObjectCreateExtension {

	private String beType = "GSPBusinessEntity";

	// region ObjectCreateExtension
	@Override
	public CommonData newInstance(String configId) {
		// 无Session调用
		return CefBeanUtil.getAppCtx().getBean(ILcpFactory.class).createCMManager(configId).createData("");
		// 有Session调用
//		return LcpUtil.getLcp(configId).createData("");
	}

	@Override
	public String getCreatorType() {
		return beType;
	}

	// endregion
}
