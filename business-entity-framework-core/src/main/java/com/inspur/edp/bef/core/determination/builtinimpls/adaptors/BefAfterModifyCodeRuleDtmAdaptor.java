/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination.builtinimpls.adaptors;

import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.core.be.BENodeEntity;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.spi.entity.CodeRuleInfo;
import com.inspur.edp.cdp.coderule.api.CodeRuleBEAssignService;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.determination.IDetermination;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.HashMap;
import java.util.Map;

public class BefAfterModifyCodeRuleDtmAdaptor implements IDetermination {

  @Override
  public String getName() {
    return null;
  }

  @Override
  public boolean canExecute(IChangeDetail iChangeDetail) {
    if(iChangeDetail.getChangeType()==ChangeType.Deleted) {
      return true;
    }
    return false;
  }

  @Override
  public void execute(ICefDeterminationContext iCefDeterminationContext,
      IChangeDetail iChangeDetail) {

    IDeterminationContext determinationContext= (IDeterminationContext) iCefDeterminationContext;
    if(determinationContext.getBEContext().getTransactionalChange()!=null &&determinationContext.getBEContext().getTransactionalChange().getChangeType()==ChangeType.Added) {
      HashMap<String, CodeRuleInfo> codeRuleMap = getCodeRule(determinationContext);

      if (codeRuleMap == null || codeRuleMap.size() == 0)
        return;

      IEntityData childData = getData(determinationContext);

      HashMap<String, Object> dataMap = getDataMap(determinationContext);

      String beID = getBeID(determinationContext);

      CodeRuleBEAssignService service = getCodeRuleService();

      for (Map.Entry<String, CodeRuleInfo> item : codeRuleMap.entrySet()) {

        service.release(beID, ((IDeterminationContext) iCefDeterminationContext).getNodeCode(),
            item.getKey(), dataMap, item.getValue().getCodeRuleId(),
            childData.getValue(item.getKey()).toString());
      }
    }
  }

  private  HashMap<String, CodeRuleInfo> getCodeRule(IDeterminationContext determinationContext) {
    BENodeEntity entity=getBENodeEntity(determinationContext);
    HashMap<String, CodeRuleInfo> codeRuleMap=entity.getAfterCreateCodeRule();
    return codeRuleMap;
  }

  private BENodeEntity getBENodeEntity(IDeterminationContext determinationContext) {
    return  (BENodeEntity) determinationContext.getBENodeEntity();
  }

  private CodeRuleBEAssignService getCodeRuleService() {
    CodeRuleBEAssignService service= SpringBeanUtils.getBean(CodeRuleBEAssignService.class);
    return service;
  }

  private String getBeID(IDeterminationContext determinationContext) {
    String beID=getRootParentContext(determinationContext.getBEContext()).getBEManagerContext().getBEManager().getBEInfo().getBEID();
    return beID;
  }

  private CoreBEContext getRootParentContext(IBENodeEntityContext beNodeEntityContext) {

    return (CoreBEContext)getParentContext(beNodeEntityContext);
  }

  private IBENodeEntityContext getParentContext(IBENodeEntityContext beNodeEntityContext){
    if(beNodeEntityContext.getParentContext() == null)
      return beNodeEntityContext;
    return getParentContext(beNodeEntityContext.getParentContext());
  }

  private IEntityData getData(IDeterminationContext determinationContext) {
    return determinationContext.getBEContext().getTransactionData();
  }

  private HashMap<String, Object> getDataMap(IDeterminationContext determinationContext) {
    IEntityData childData= determinationContext.getBEContext().getTransactionData();
    HashMap<String,Object> dataMap=new HashMap<>();
    addparentData(dataMap,determinationContext.getBEContext());
    dataMap.put(determinationContext.getBEContext().getCode(),childData);
    return dataMap;
  }

  private void addparentData(HashMap<String,Object> dataMap, IBENodeEntityContext context){
    if (context.getParentContext() == null) {
      return;
    }
    dataMap.put(context.getParentContext().getCode(),context.getParentContext().getTransactionData());

    addparentData(dataMap,context.getParentContext());

  }
}
