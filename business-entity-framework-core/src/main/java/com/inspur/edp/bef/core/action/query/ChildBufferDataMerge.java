/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.query;

import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBENodeEntity;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.cef.entity.changeset.AddChangeDetail;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.DeleteChangeDetail;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

// 子表
public class ChildBufferDataMerge {
  private EntityFilter filter;
  private String nodeCode;
  private IBEManagerContext mgrContext;
  private java.util.List<IEntityData> dataList;
  private PaginationDataMerge beMerger;

  public ChildBufferDataMerge(
      EntityFilter filter,
      String nodeCode,
      IBEManagerContext mgrContext,
      java.util.List<IEntityData> dataList) {
    this.filter = filter;
    this.nodeCode = nodeCode;
    this.mgrContext = mgrContext;
    this.dataList = dataList;
    beMerger = new PaginationDataMerge(filter, dataList);
  }

  public final void merge() {
List<IBusinessEntity> entities = mgrContext.getAllEntities();
    if (entities == null || entities.size() == 0) {
      return;
    }

for (IBusinessEntity item : entities) {
      merge(item, ChangeType.Modify);
    }
  }

  private void merge(IBENodeEntity nodeEntity, ChangeType parentChangeType) {
    if (parentChangeType.equals(ChangeType.Deleted)) {
      mergeByDelete(nodeEntity);
    } else if (parentChangeType.equals(ChangeType.Added)) {
      mergeByAdd(nodeEntity);
    } else if (parentChangeType.equals(ChangeType.Modify)) {
      mergeByModify(nodeEntity);
    } else {
    }
  }

  private void mergeByModify(IBENodeEntity nodeEntity) {
    IChangeDetail currentChange = beMerger.getOwnChange(nodeEntity);
    if (currentChange instanceof AddChangeDetail) {
      mergeByAdd(nodeEntity);
    } else if (currentChange instanceof DeleteChangeDetail) {
      mergeByDelete(nodeEntity);
    } else if (currentChange instanceof ModifyChangeDetail) {
      ModifyChangeDetail modifyChange = (ModifyChangeDetail) currentChange;
      Map<String, Map<String, IChangeDetail>> childsChanges = modifyChange.getChildChanges();
      if (childsChanges != null) {
// keyword - these cannot com.inspur.edp.bef.core.be converted using the 'RefObject' helper
        // class unless the method is within the code being modified:
        Map<String, IChangeDetail> targetChildChanges = childsChanges.get(nodeCode);
        if (targetChildChanges != null) {
for (IChangeDetail childChange : targetChildChanges.values()) {
            if (childChange.getChangeType() == ChangeType.Deleted) {
              beMerger.mergeFromDelete(childChange.getDataID());
            } else {
              beMerger.merge(
                  ((IBENodeEntityContext) nodeEntity.getContext())
                      .getCurrentData()
                      .getChilds().get(nodeCode).getItem(childChange.getDataID()),
                  Optional.of(childChange.getChangeType()));
            }
          }
        } else {
for (Map.Entry<String, Map<String, IChangeDetail>> childChanges :
              childsChanges.entrySet()) {
for (Map.Entry<String, IChangeDetail> childChange :
                childChanges.getValue().entrySet()) {
              merge(
                  nodeEntity.getChildBEEntity(childChanges.getKey(), childChange.getKey()),
                  ChangeType.Modify);
            }
          }
        }
      }
    }
  }

  private void mergeByDelete(IBENodeEntity nodeEntity) {
    IEntityData currentData = ((IBENodeEntityContext) nodeEntity.getContext()).getOriginalData();
    java.util.Map<String, IEntityDataCollection> childs = currentData.getChilds();
    if (childs == null || childs.isEmpty()) {
      return;
    }
    IEntityDataCollection child = childs.get(nodeCode);
    if (child != null) {
for (IEntityData childItem : child) {
IBENodeEntity childEntity = nodeEntity.getChildBEEntity(nodeCode, childItem.getID());
        beMerger.mergeFromDelete(childItem);
      }
    } else {
for (Map.Entry<String, IEntityDataCollection> childPair : childs.entrySet()) {
for (IEntityData childItem : childPair.getValue()) {
          merge(
              nodeEntity.getChildBEEntity(childPair.getKey(), childItem.getID()),
              ChangeType.Deleted);
        }
      }
    }
  }

  private void mergeByAdd(IBENodeEntity nodeEntity) {
    IEntityData currentData = ((IBENodeEntityContext) nodeEntity.getContext()).getCurrentData();
    java.util.Map<String, IEntityDataCollection> childs = currentData.getChilds();
    if (childs == null || childs.isEmpty()) {
      return;
    }
    IEntityDataCollection child = childs.get(nodeCode);
    if (child != null) {
for (IEntityData childItem : child) {
IBENodeEntity childEntity = nodeEntity.getChildBEEntity(nodeCode, childItem.getID());
        beMerger.mergeFromAdd(childItem);
      }
    } else {
for (Map.Entry<String, IEntityDataCollection> childPair : childs.entrySet()) {
for (IEntityData childItem : childPair.getValue()) {
          merge(
              nodeEntity.getChildBEEntity(childPair.getKey(), childItem.getID()), ChangeType.Added);
        }
      }
    }
  }
}
