/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.clear;


import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.bef.spi.action.assembler.entityAssemblerFactory.IDefaultEntityActionAssFactory;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public class ClearChangesetAction extends AbstractAction<VoidActionResult>
{
	@Override
	public void execute()
	{
		if (!ActionUtil.getBEContext(this).hasData())
		{
			return;
		}
		ActionUtil.getBEContext(this).getBizEntity().onCancelDeterminate();
		ActionUtil.getBEContext(this).rejectTransactional();
	}

	@Override
	protected IBEActionAssembler getAssembler()
	{
		return ((IDefaultEntityActionAssFactory)getAssemblerFactory()).getDeleteActionAssembler(ActionUtil.getBEContext(this));
	}
}
