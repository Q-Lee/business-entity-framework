/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.serialize;

import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.io.IOException;
import lombok.var;

public class EntityDataDeserMgrAction extends AbstractManagerAction<IEntityData> {

   private String data;

   public EntityDataDeserMgrAction(IBEManagerContext managerContext, String data) {
      super(managerContext);
      this.data = data;
   }

   @Override
   public void execute() {
      var mapper = ((BEManager) getBEManagerContext().getBEManager()).getDataJsonMapper();
      try {
         setResult((IEntityData) mapper.readValue(data, ICefData.class));
      } catch (IOException e) {
         throw new RuntimeException(e);
      }
   }
}
