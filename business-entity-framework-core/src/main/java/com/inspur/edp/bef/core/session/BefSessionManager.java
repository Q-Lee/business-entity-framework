/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session;

import com.inspur.edp.bef.core.session.distributed.EditableBefSessionManager;
import com.inspur.edp.bef.api.services.IBefSessionManager;
import com.inspur.edp.bef.core.DotNetToJavaStringHelper;
import com.inspur.edp.cef.api.session.ICefSession;
import com.inspur.edp.cef.api.session.ICefSessionManager;
import com.inspur.edp.commonmodel.core.session.distributed.SessionEditToken;
import java.time.Duration;
import java.util.Objects;

public class BefSessionManager implements IBefSessionManager, ICefSessionManager,
		EditableBefSessionManager
{
	@Override
	@Deprecated
	public final String createSession(){
		return FuncSessionManager.getCurrent().initSession().getSessionId();
  }

	@Override
	public String createSession(Duration duration) {
		return FuncSessionManager.getCurrent().initSession(duration).getSessionId();
	}

	@Override
	public final void closeCurrentSession()
	{
		String currId = FuncSessionManager.getCurrentFuncSessionId();
		if (DotNetToJavaStringHelper.isNullOrEmpty(currId)) {
			throw new RuntimeException("当前不存在SessionId");
		}
		FuncSessionManager.getCurrent().closeSession(currId);
		//Console.writeLine("BESession已关闭: " + currId);
		//FuncSessionManager.clearCurrentFuncSessionId();
	}

	@Override
	public final String getCurrentSessionId()
	{
		return FuncSessionManager.getCurrentFuncSessionId();
	}

	@Override
	public ICefSession getCurrentSession() {
		return FuncSessionManager.getCurrentSession();
	}

	//region editing
	@Override
	public String createDistributedSession() {
		return FuncSessionManager.getCurrent().initSession(null, true).getSessionId();
	}

//	@Override
//	public SessionEditToken beginEdit(String sessionId) {
//		Objects.requireNonNull(sessionId, "sessionId不能为null");
//
//		FuncSession session = FuncSessionManager.getCurrent().getSession(sessionId);
//		if(session == null) {
//			throw new RuntimeException("befsession不存在,它可能已被关闭:" + sessionId);
//		}
//		return session.beginEdit();
//	}

	@Override
	public SessionEditToken beginEdit() {
		return FuncSessionManager.getCurrent().beginEdit();
//		FuncSession session = FuncSessionManager.getCurrentSession();
//		if(session == null) {
//			throw new RuntimeException();
//		}
//		return session.beginEdit();
	}

	@Override
	public void endEdit(SessionEditToken token) {
		Objects.requireNonNull(token ,"token不能为null");
		FuncSessionManager.getCurrent().endEdit(token);
//		FuncSession session = FuncSessionManager.getCurrent().getSession(token.getSessionId());
//		if(session.getCurrentEditToken() != token) {
//			throw new RuntimeException("token mismatched");
//		}
//		session.endEdit(token);
	}

	//endregion
}
