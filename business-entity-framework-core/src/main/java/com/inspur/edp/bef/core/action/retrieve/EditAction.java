/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrieve;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.core.lock.LockUtils;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.cef.api.action.EditResult;
import com.inspur.edp.cef.entity.accessor.base.AccessorComparer;
import com.inspur.edp.cef.entity.entity.IMultiLanguageData;
import java.util.Date;

public class EditAction extends AbstractAction<EditResult> {

  public EditAction(IBEContext beContext) {
  }

  @Override
  public final void execute() {
    setResult(new EditResult());
    CoreBEContext beContext = ActionUtil.getBEContext(this);
    BusinessEntity beEntity = ActionUtil.getRootEntity(this);
    if (beContext.isLocked()) {
      return;
    }

    boolean hasData = beContext.hasData();
    Date orgVersion =
        beContext.getData() != null ? beEntity.getVersionControlPropValue() : new Date(0);
    RetrieveParam retrieveParam = new RetrieveParam();
    retrieveParam.setNeedLock(true);
    if(beContext.getData() != null && beContext.getData() instanceof IMultiLanguageData
        && !((IMultiLanguageData) beContext.getData()).getMultiLanguageInfos().isEmpty())
      retrieveParam.getRetrieveFilter().setEnableMultiLanguage(true);
    RespectiveRetrieveResult retrieveResult = beEntity.retrieve(retrieveParam);
    if (retrieveResult.getData() == null) {
      return;
    }
    LockUtils.checkLocked(beContext);
    Date version = beEntity.getVersionControlPropValue();
    if (!hasData || !AccessorComparer.equals(orgVersion, version)) {
      getResult().setData(retrieveResult.getData());
    }
  }
}
