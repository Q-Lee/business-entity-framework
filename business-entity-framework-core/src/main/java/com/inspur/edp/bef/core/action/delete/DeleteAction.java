/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.delete;


import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.exceptions.BefException;
import com.inspur.edp.bef.api.exceptions.ErrorCodes;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.core.lock.LockUtils;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.cef.entity.changeset.DeleteChangeDetail;

public class DeleteAction extends AbstractAction<VoidActionResult>
{
	@Override
	public void execute()
	{
		//DemandDeletePermission(Context);
		if(ActionUtil.getBEContext(this).getCurrentData() == null)
		{
			throw new BefException(ErrorCodes.DeletingNonExistence, "要删除的数据不存在", null, ExceptionLevel.Error);
		}
		LockUtils.checkLock(ActionUtil.getBEContext(this));
		AuthorityUtil.checkDataAuthority(ActionUtil.getBEContext(this));
		ActionUtil.getRootEntity(this).appendTempCurrentChange(new DeleteChangeDetail(ActionUtil.getBEContext(this).getID()));
		//CoreBEContext.ChangeFromModify = new DeleteChangeDetail(CoreBEContext.ID);
		//CoreBEContext.acceptChangeFromModify();
	}

	@Override
	protected IBEActionAssembler getAssembler()
	{
		return GetAssemblerFactory().getDeleteActionAssembler(ActionUtil.getBEContext(this));
	}

	//internal static void DemandDeletePermission(IBEContext beCtx)
	//{
	//    if (beCtx.isNew())
	//        return;
	//    var com.inspur.edp.bef.core.assembler = ((IDefaultEntityActionAssFactory)beCtx.AbstractActionAssemblerFactory).
	//        GetDeleteActionAssembler(beCtx);
	//    if (com.inspur.edp.bef.core.assembler == null) return;
	//    if (!com.inspur.edp.bef.core.assembler.demandDataPermission(beCtx.OriginalData))
	//        throw new BefDataPermissionDeniedException();
	//}
}
