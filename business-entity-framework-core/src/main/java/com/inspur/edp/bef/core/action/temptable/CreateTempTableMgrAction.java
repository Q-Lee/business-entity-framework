/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.temptable;

import com.inspur.edp.bef.api.lcp.BefTempTableContext;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import java.util.List;

public class CreateTempTableMgrAction extends AbstractManagerAction<BefTempTableContext> {

  private List<String> nodeCodes;

  public CreateTempTableMgrAction(List<String> nodeCodes)
  {
super.isReadOnly=true;
    this.nodeCodes = nodeCodes;
  }
  @Override
  public void execute() {
    BefTempTableContext befTempTableContext=new BefTempTableContext();
    for(String nodeCode:nodeCodes)
    {
      befTempTableContext.getDboContexts().put(nodeCode,getBEManagerContext().getBEManager().getRepository().createTempTable(nodeCode));
    }
    setResult(befTempTableContext);
  }
}
