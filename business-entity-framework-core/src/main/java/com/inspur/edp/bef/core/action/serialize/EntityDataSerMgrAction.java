/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.serialize;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.bef.spi.entity.AbstractBizEntitySerializer;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import lombok.var;

public class EntityDataSerMgrAction extends AbstractManagerAction<String> {
  private IEntityData data;
    private final boolean keepAssoPropertyForExpression;

    public EntityDataSerMgrAction(IBEManagerContext managerContext, IEntityData data) {
    this(managerContext,data,false);
  }

    public EntityDataSerMgrAction(IBEManagerContext managerContext, IEntityData data,boolean keepAssoPropertyForExpression) {
        super(managerContext);
        this.data = data;
        this.keepAssoPropertyForExpression = keepAssoPropertyForExpression;
    }

  @Override
  public void execute() {

      ObjectMapper mapper = null;
      if(keepAssoPropertyForExpression)
          mapper=((BEManager) getBEManagerContext().getBEManager()).getDataJsonMapperKeepAssoPropertyForExpr();
      else
          mapper=((BEManager) getBEManagerContext().getBEManager()).getDataJsonMapper();
    try {
      setResult(mapper.writeValueAsString(data));
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
  }
}
