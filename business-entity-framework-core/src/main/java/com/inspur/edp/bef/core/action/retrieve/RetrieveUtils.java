/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrieve;

import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.api.parameter.retrieve.RequestedBufferType;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.determination.BeforeRetrieveDtmExecutor;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;
import java.util.List;

class RetrieveUtils {

   public static void executeBeforeRetrieve(IBEManagerContext mgrContext, List<String> idList,
       RetrieveParam para) {
      BeforeRetrieveDtmExecutor executor = new BeforeRetrieveDtmExecutor();
      IEntityRTDtmAssembler assembler = ((BEManager) (mgrContext.getBEManager()))
          .getBeforeRetrieveDtmAssembler();
       if (assembler == null) {
           return;
       }
      executor.execute(assembler, mgrContext, idList, para);
   }

   public static IEntityData getData(IBENodeEntityContext beContext, RequestedBufferType type) {
       switch (type)
       {
           case CurrentData:
               return beContext.getCurrentData();
           case TransactionalBuffer:
               return beContext.getTransactionData();
           case Original:
               return beContext.getOriginalData();
           default:
               throw new RuntimeException();
       }
   }
}
