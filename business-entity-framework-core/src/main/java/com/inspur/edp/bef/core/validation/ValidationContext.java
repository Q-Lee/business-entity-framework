/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.validation;

import com.inspur.edp.bef.api.action.assembler.IValidationAssemblerFactory;
import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.core.be.BEContext;
import com.inspur.edp.bef.core.be.BusinessEntity;
import com.inspur.edp.bef.core.determination.DeterminationContext;
import com.inspur.edp.cef.entity.entity.ICefData;

public class ValidationContext extends DeterminationContext implements IValidationContext
{
	public ValidationContext(IBENodeEntityContext beContext)
	{
		super(beContext);
	}

	private IValidationAssemblerFactory privateValidationFactory;
	public final IValidationAssemblerFactory getValidationFactory()
	{
		return privateValidationFactory;
	}
	public final void setValidationFactory(IValidationAssemblerFactory value)
	{
		privateValidationFactory = value;
	}

	@Override
	public IBEManagerContext getBEManagerContext(){return ((BusinessEntity)((getRootContext(getBEContext()).getBizEntity() instanceof BusinessEntity) ? getRootContext(getBEContext()).getBizEntity() : null)).getBEContext().getBEManagerContext();
	}

	private IBEContext getRootContext(IBENodeEntityContext entityContext)
	{
		if (entityContext instanceof IBEContext)
		{
			return (IBEContext)((entityContext instanceof IBEContext) ? entityContext : null);
		}
		return getRootContext(entityContext.getParentContext());
	}

	@Override
	public ICefData getData (){return getCurrentData();}
}
