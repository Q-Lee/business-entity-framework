/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.save.saveEvent;

import com.inspur.edp.bef.api.be.BEInfo;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.util.ArrayList;
import org.springframework.util.StringUtils;

public class SaveEventBroker extends EventBroker {
    private SaveEventManager eventManager = SpringBeanUtils.getBean(SaveEventManager.class);

//     @Autowired
//    private static SaveEventBroker saveEventBroker;

    public SaveEventBroker(EventListenerSettings settings) {
        super(settings);
        this.init();
    }

    @Override
    protected void onInit() {
        // eventManager = new SaveEventManager();
        this.getEventManagerCollection().add(eventManager);
    }

    private void fireBeforeSave(BEInfo beInfo, String funcSessionID, java.util.ArrayList<String> dataIds,
                                java.util.ArrayList<IChangeDetail> changes) {
        eventManager.fireBeforeSave(beInfo, funcSessionID, dataIds, changes);
    }

    private void fireAfterSave(BEInfo beInfo, String funcSessionID, java.util.ArrayList<String> dataIds,
                               java.util.ArrayList<IChangeDetail> changes) {
        eventManager.fireAfterSave(beInfo, funcSessionID, dataIds, changes);
    }

    public void fireBeforeSave(BEManager beMgr) {
        if(beMgr.getBEInfo() == null || beMgr.getBEInfo().getBEID() == null || beMgr.getBEInfo().getBEID().isEmpty()){
            return;
        }
        RefObject<ArrayList<IBusinessEntity>> entities = new RefObject<>((ArrayList<IBusinessEntity>) beMgr.getBEManagerContext().getAllEntities());
        java.util.ArrayList<String> arrayList = new java.util.ArrayList<>(entities.argvalue.size());
        RefObject<java.util.ArrayList<String>> dataIds = new RefObject<>(arrayList);
        java.util.ArrayList<IChangeDetail> changeDetails = new java.util.ArrayList<>(
            entities.argvalue.size());
        RefObject<java.util.ArrayList<IChangeDetail>> changes = new RefObject<>(changeDetails);
        getSaveEventArgs(beMgr,entities,dataIds, changes);
        FuncSession funcSession = beMgr.getBEManagerContext().getSession();
        boolean setSavingFlag = false;
        if(!funcSession.getBefContext().getIsBeforeSaving()) {
            setSavingFlag = true;
            funcSession.getBefContext().setIsBeforeSaving(true);
        }
        try {
            fireBeforeSave(beMgr.getBEInfo(), funcSession.getSessionId(), dataIds.argvalue, changes.argvalue);
        } finally {
            if(setSavingFlag)
                funcSession.getBefContext().setIsBeforeSaving(false);
        }
    }

    public void fireAfterSave(BEManager beMgr) {
        if(beMgr.getBEInfo() == null || StringUtils.isEmpty(beMgr.getBEInfo().getBEID())){
            return;
        }
        RefObject<ArrayList<IBusinessEntity>> entities = new RefObject<>((ArrayList<IBusinessEntity>) beMgr.getBEManagerContext().getAllEntities());
        java.util.ArrayList<String> arrayList = new ArrayList<>(entities.argvalue.size());
        RefObject<ArrayList<String>> dataIds = new RefObject<>(arrayList);
        java.util.ArrayList<IChangeDetail> changeDetails = new ArrayList<>(entities.argvalue.size());
        RefObject<ArrayList<IChangeDetail>> changes = new RefObject<>(changeDetails);
        getSaveEventArgs(beMgr, entities, dataIds, changes);
        fireAfterSave(beMgr.getBEInfo(), beMgr.getBEManagerContext().getSession().getSessionId(),
            dataIds.argvalue, changes.argvalue);
    }

    private void getSaveEventArgs(BEManager beMgr, RefObject<ArrayList<IBusinessEntity>> entities, RefObject<ArrayList<String>> dataIds, RefObject<ArrayList<IChangeDetail>> changes) {
        for (IBusinessEntity item : entities.argvalue) {
            dataIds.argvalue.add(item.getID());
            if (item.getBEContext().getCurrentTransactionalChange() != null) {
                changes.argvalue.add(item.getBEContext().getCurrentTransactionalChange());
            }
        }
    }
}
