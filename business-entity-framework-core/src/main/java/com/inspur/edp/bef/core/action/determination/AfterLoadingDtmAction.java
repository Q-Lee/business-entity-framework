/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.determination;

import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.determination.BeDeterminationTypes;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.cef.core.determination.NoChangesetEntityDtmExecutor;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;
import com.inspur.edp.cef.spi.extend.entity.ICefEntityExtend;

import java.util.List;

public class AfterLoadingDtmAction extends AbstractAction<VoidActionResult>
{
	//ICoreDeterminContext IDtmExecutionAction.DtmContext => context;

	private List<ICefEntityExtend> extList;
	public AfterLoadingDtmAction(IBENodeEntityContext context, List<ICefEntityExtend> extList)
	{
		this.extList = extList;
	}

	@Override
	public void execute()
	{
IEntityRTDtmAssembler ass = ActionUtil.getBENodeEntity(this).getAfterLoadingDtmAssembler();
		if (ass == null)
		{
			return;
		}

		if(extList != null){
			for(ICefEntityExtend ext:extList){
				ext.addExtDtm(ass, BeDeterminationTypes.AfterLoading);
			}
		}

		new NoChangesetEntityDtmExecutor(ass, getBEContext()).execute();

		//IDtmAssemblerFactory dtmFactory = BEContext.DtmAssemblerFactory;
		//if (dtmFactory == null)
		//    return;
		//context.DtmFactory = dtmFactory;
		//var com.inspur.edp.bef.core.assembler = dtmFactory.getAfterLoadingDtmAssembler();
		//if (com.inspur.edp.bef.core.assembler == null)
		//    return;
		//context.CompContext = new DeterminationContext(BEContext);
		//new AfterLoadingDtmExecutor(context, com.inspur.edp.bef.core.assembler, BEContext.CurrentData).execute();

		//把修改合并到t和o缓存上
		//CoreBEContext.acceptTempChangeDetail();
		if (getIsRootAction())
		{
			ActionUtil.getBEContext(this).acceptListenerChange();
			ActionUtil.getBEContext(this).acceptChanges();
			ActionUtil.getBEContext(this).acceptTransaction();
		}
	}

	private boolean getIsRootAction(){return (getBEContext() instanceof CoreBEContext) ;}
}
