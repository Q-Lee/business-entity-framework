/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.changeset;

import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.cef.core.changesetmgr.ChangesetManager;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

//
//     *  现在ModifyAction里调用BeContext.AcceptChangeFromModify, 合并前会suspendlistener;
//     *  是AfterModifyDtmAction里MergeInnerChange;
//     *  触发AfterModify时机 联动计算/校验规则 这两种变更是合并起来触发的, 合并到CurrentChange也一样;
//     *  只是InnerChange需要区分开;
//     *  可以改成templateChange通过监听往changesetManager合并, 合并ChangeFromModify的时候暂停监听
//     *  直接往currentChange合并?
//     *
//     *  综上: changesetManager上就三个, Tran/Current/Template(触发dtm/com.inspur.edp.bef.core.validation)
//     *  比如:
//     *  a. modify进来的, 暂停监听合并到TempCurrent; √
//     *  b. 监听到的变更(即非Modify产生的变更)往Inner/TempCurrent合并; √
//     *  c. 在执行完Determination之后把TempCurrent合并到CurrentChange; √
//     *  d. 此处认为自定义动作中不会调用modify否则一个自定义动作内部再调用Modify传的变更应该是合并进内部变更的但是这种方式识别不出来,
// 最好从接口上控制住Modify调用或者其实这是一个feature?;
//     *
//     *  3 ListenerChange
//     *  2 CurrentTemplateChange
//     *  1 CurrentChange
//     *  0 TransactionChange
//
public class BEChangesetManager extends ChangesetManager {
  public BEChangesetManager(String beType, FuncSession session) {
    super(beType, session);
  }

public IChangeDetail getCurrentTemplateChange(String id) {
    return getChange(id, 2);
  }

public IChangeDetail getCurrentChange(String id) {
    return getChange(id, 1);
  }

public IChangeDetail getTransactionChange(String id) {
    return getChange(id, 0);
  }

public void acceptCurrentTemplateChange(String id) {
    setChange(id, 2, null);
  }

public void acceptCurrentChange(String id) {
    acceptChange(id, 1);
  }

  public final void acceptTransactionChange(String id) {
    // 没有OriginalChange, TransactionChange是0层, 0层在基类里不能再向上Accept, 需要特殊处理;
    rejectChange(id, 0);
  }

  public final void appendCurrentTemplateChange(IChangeDetail change) {
    appendChange(change.getDataID(), 2, change);
    appendChange(change.getDataID(), 1, change);
  }

  // public void appendCurrentChange(IChangeDetail change)
  // {
  //    AppendChange(change.getID(), 1, change);
  // }

  public final void rejectCurrentTemplateChange(String id) {
    rejectChange(id, 3);
    rejectChange(id, 2);
  }

  public void rejectCurrentChange(String id) {
    rejectChange(id, 1);
  }

  public void rejectTransaction(String id) {
    rejectChange(id, 0);
  }

  public final void initCurrentTemplateChange(IChangeDetail change) {
    appendChange(change.getDataID(), 2, change);
  }

  public void initListenerChange(IChangeDetail change) {
    appendChange(change.getDataID(), 3, change);
  }

  public IChangeDetail getListenerChange(String id) {
    return getChange(id, 3);
  }

  public final void acceptListenerChange(String ID) {
    IChangeDetail listenerChange = getListenerChange(ID);
    if (listenerChange == null) {
      return;
    }
    appendCurrentTemplateChange(listenerChange);
    setChange(ID, 3, null);
  }

  public void rollBack(String id, IChangeDetail change)
  {
    setChange(id, 3, null);
    setChange(id, 2, null);
    setChange(id, 1, null);
    setChange(id, 0, change);
  }
}
