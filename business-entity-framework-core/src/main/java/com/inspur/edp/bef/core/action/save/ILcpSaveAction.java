/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.save;

import com.inspur.edp.bef.api.parameter.save.SaveParameter;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.caf.transaction.api.annoation.tcc.BusinessActionContext;
import com.inspur.edp.caf.transaction.api.annoation.tcc.LocalTCC;
import com.inspur.edp.caf.transaction.api.annoation.tcc.TwoPhaseBusinessAction;
import java.sql.SQLException;
import javax.transaction.Transactional;

@LocalTCC
public interface ILcpSaveAction {

  @TwoPhaseBusinessAction(name = "BefTcc", confirmMethod = "onTccConfirm", cancelMethod = "onTccCancel")
  void execute(BusinessActionContext tccCtx, FuncSession session, BEManager lcp, SaveParameter saveParam);

  void onTccConfirm(BusinessActionContext tccContext) throws SQLException;

  void onTccCancel(BusinessActionContext tccContext) throws SQLException;
}
