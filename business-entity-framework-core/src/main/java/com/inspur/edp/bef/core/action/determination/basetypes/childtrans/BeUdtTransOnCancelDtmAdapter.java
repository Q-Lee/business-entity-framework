/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.determination.basetypes.childtrans;

import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObject;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.basetypes.nestedTrans.CefNestedTransDtmAdapter;

public class BeUdtTransOnCancelDtmAdapter extends CefNestedTransDtmAdapter {

  public BeUdtTransOnCancelDtmAdapter(String name, String propertyName, String udtConfig) {
    super(name, propertyName, udtConfig);
  }

  @Override
  public boolean canExecute(IChangeDetail iChangeDetail) {
    return false;
  }

  @Override
  protected void doExecute(ICefValueObject iCefValueObject, ICefDeterminationContext parent, IChangeDetail var2) {

  }
}
