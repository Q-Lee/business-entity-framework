/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.clear;


import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import java.util.ArrayList;
import java.util.List;

public class ClearChangesetMgrAction extends AbstractManagerAction<VoidActionResult> {

  private String dataId;

  public ClearChangesetMgrAction(IBEManagerContext managerContext) {
    super(managerContext);
  }

  @Override
  public final void execute() {
    List<IBusinessEntity> addingEntity = new ArrayList<>();
    for (IBusinessEntity entity : getBEManagerContext().getAllEntities()) {
      if (entity.getBEContext() != null && entity.getBEContext().isNew()) {
        addingEntity.add(entity);
      }
      entity.clearChangeset();
    }
    for (IBusinessEntity entity : addingEntity) {
      ((BEManager) getBEManagerContext().getBEManager()).removeEntity(entity.getID());
    }
    ((BEManagerContext)getBEManagerContext()).getSessionItem().getBufferChangeManager().notifyReset();
  }
}
