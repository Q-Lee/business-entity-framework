/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session;


import com.inspur.edp.bef.core.session.distributed.close.AbnormalClosingUtil;
import io.iec.edp.caf.runtime.event.FrameworkEventArgs;
import io.iec.edp.caf.runtime.event.FunctionEventArgs;
import io.iec.edp.caf.runtime.event.IFrameworkEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * 响应销毁Session事件的处理类
 */

public class ResponseSession implements IFrameworkEventListener {

  private static Logger logger = LoggerFactory.getLogger(ResponseSession.class);

  static {
    SessionCacheCheckInSys.startCleanerScheduler();
  }

  @Override
  public void functionActive(FunctionEventArgs functionEventArgs) {
  }

  @Override
  public void functionDeActive(FunctionEventArgs functionEventArgs) {
  }

  @Override
  public void functionEntered(FunctionEventArgs functionEventArgs) {
    String token = functionEventArgs.commonVariableToken;
    if (!StringUtils.isEmpty(token)) {
      AbnormalClosingUtil.getInstance().abnormalFunctionClosing(token);
    }
//    FuncSessionManager.getCurrent().closeSessionWithToken(token);
  }

  @Override
  public void functionEntering(FunctionEventArgs functionEventArgs) {
  }

  @Override
  public void functionQuited(FunctionEventArgs functionEventArgs) {
  }

  @Override
  public void functionQuiting(FunctionEventArgs functionEventArgs) {

    String token = functionEventArgs.commonVariableToken;
    if (!StringUtils.isEmpty(token)) {
      AbnormalClosingUtil.getInstance().abnormalFunctionClosing(token);
    }
//    FuncSessionManager.getCurrent().closeSessionWithToken(token);
  }

  @Override
  public void userInitializing(FrameworkEventArgs functionEventArgs) {

  }

  @Override
  public void userLogouted(FrameworkEventArgs functionEventArgs) {

  }

  @Override
  public void userLogouting(FrameworkEventArgs functionEventArgs) {

  }

  @Override
  public void userLogined(FrameworkEventArgs frameworkEventArgs) {

  }

  @Override
  public void userLogining(FrameworkEventArgs frameworkEventArgs) {

  }
}
