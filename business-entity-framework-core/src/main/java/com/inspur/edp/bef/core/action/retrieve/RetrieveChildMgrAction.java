/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrieve;

import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.parameter.retrieve.RespectiveRetrieveResult;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import java.util.List;

public class RetrieveChildMgrAction extends AbstractManagerAction<RespectiveRetrieveResult>
{

		//region Consturctor

	public RetrieveChildMgrAction(IBEManagerContext managerContext, List<String> nodeCodes,
			List<String> hierachyIdList, RetrieveParam para) {
		super(managerContext);
		ActionUtil.requireNonNull(nodeCodes, "nodeCodes");
		ActionUtil.requireNonNull(hierachyIdList, "hierachyIdList");

		this.nodeCodes = nodeCodes;
		this.hierachyIdList = hierachyIdList;
		this.para = para;
	}


		//endregion Consturctor


		//region 字段属性
	private RetrieveParam para;
	private java.util.List<String> nodeCodes;
	private java.util.List<String> hierachyIdList;

		//endregion 字段属性


		//region Override

	@Override
	public final void execute()
	{
		if (nodeCodes.size() != hierachyIdList.size() - 1)
		{
			throw new IllegalArgumentException("子表编号与id不匹配");
		}

		getBEManagerContext().checkAuthority("Retrieve");
//		AuthorityUtil.checkAuthority("Retrieve");
//		FuncSessionManager.getCurrentSession().getBefContext().setCurrentOperationType("Retrieve");
		IBusinessEntity be = getBEManagerContext().getEntity(hierachyIdList.get(0));
		be.retrieve(para);
		setResult(be.retrieveChild(nodeCodes, hierachyIdList, para));
	}

@Override
	protected final IMgrActionAssembler getMgrAssembler() { return null; }

		//endregion Override


		//region private

	//private void setRetrieveResult(IBusinessEntity com.inspur.edp.bef.core.be)
	//{
	//	RetrieveResult = new RetrieveResult();
	//	RetrieveResult.DicAccessor.add(com.inspur.edp.bef.core.be.BEContext.ID, com.inspur.edp.bef.core.be.BEContext.CurrentData);
  //          if (!com.inspur.edp.bef.core.be.BEContext.isLocked())
  //              RetrieveResult.LockFailedIds.add(com.inspur.edp.bef.core.be.ID);
	//}

		//endregion
}
