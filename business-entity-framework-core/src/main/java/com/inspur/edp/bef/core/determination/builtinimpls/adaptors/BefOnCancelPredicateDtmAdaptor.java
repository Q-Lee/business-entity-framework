/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination.builtinimpls.adaptors;

import com.inspur.edp.bef.core.determination.builtinimpls.predicate.ExecutePredicate;

public class BefOnCancelPredicateDtmAdaptor extends BefAftModifyPredicateDtmAdaptor {

  public BefOnCancelPredicateDtmAdaptor(String name, Class type, ExecutePredicate predicate) {
    super(name, type, predicate, false);
  }
}
