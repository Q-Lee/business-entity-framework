/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.base;


import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import java.util.ArrayList;

public class BefActionStack
{
	private java.util.Stack stack;
	private BefActionStack preStack;
	private String funcSessionID;
	private String firstBEType;

	public BefActionStack(String funcSessionID)
	{
		this.funcSessionID = funcSessionID;
		stack = new java.util.Stack();
	}

	public BefActionStack(String funcSessionID, BefActionStack preStack)
	{
		this(funcSessionID);
		this.preStack = preStack;
	}

	public final void pushPrestack(BefActionStack preStack)
	{
		this.preStack = preStack;
	}

	public final BefActionStack popPreStack()
	{
		BefActionStack pre = this.preStack;
		this.preStack = null;
		return pre;
	}

	public final boolean hasPreStack()
	{
		return preStack != null;
	}

  public String getFirstBEType(){
		return this.firstBEType;
}

	public final void push(Object value, String beType) {
		if(stack.isEmpty()){
			this.firstBEType = beType;
		}
		stack.push(value);
	}

	private void checkSessionID() {
//		if (!equals(funcSessionID)) {
//			throw new RuntimeException("动作堆栈上下文的SessionID不一致[" + funcSessionID +"]:[" + FuncSessionManager.getCurrentFuncSessionId() + "]，请检查相关逻辑中是否创建BefSession未关闭或错误关闭外层BefSession。");
//		}
	}

	public final Object pop() {
		checkSessionID();
		return stack.pop();
	}

	public final int count() {
		return stack.size();
	}
}
