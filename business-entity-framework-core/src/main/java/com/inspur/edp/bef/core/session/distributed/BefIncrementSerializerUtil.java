/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.session.distributed;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.inspur.edp.bef.api.BefRtBeanUtil;
import com.inspur.edp.bef.api.lcp.IStandardLcp;
import com.inspur.edp.bef.core.LcpUtil;
import com.inspur.edp.bef.core.be.BEManager;
import com.inspur.edp.bef.core.session.BEFuncSessionBase;
import com.inspur.edp.cef.api.session.ICefSessionItem;
import com.inspur.edp.cef.designtime.api.json.SerializerUtils;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.commonmodel.api.ICMManager;
import com.inspur.edp.commonmodel.core.session.distributed.dac.FuncSessionItemIncrement;
import com.inspur.edp.commonmodel.core.util.SessionIncSerializerUtil;
import lombok.SneakyThrows;

import java.util.List;

public class BefIncrementSerializerUtil implements SessionIncSerializerUtil {

    @SneakyThrows
    @Override
    public ValueObjModifyChangeDetail deserializerVar(String configId, JsonParser jsonParser) {
        IStandardLcp lcp = LcpUtil.getLcp(configId);
        return (ValueObjModifyChangeDetail)lcp.deserializeChange(jsonParser.readValueAsTree().toString());
    }

    @SneakyThrows
    @Override
    public List<IChangeDetail> deserializerChanges(String configId, JsonParser jsonParser) {
        ICMManager lcp = BefRtBeanUtil.getLcpFactory().createCMManager(configId);
        return lcp.deserializeChanges(jsonParser.readValueAsTree().toString());
    }

    @SneakyThrows
    @Override
    public void serializerVar(JsonGenerator writer, FuncSessionItemIncrement increment, ICefSessionItem cefSessionItem) {
        String temp = ((BEManager)((BEFuncSessionBase)cefSessionItem).getBEManager()).getBEManagerContext().getVariableManager().serializeChange(increment.getVariable());
        SerializerUtils.writePropertyName(writer,"variable");
        writer.writeRawValue(temp);
    }

    @SneakyThrows
    @Override
    public void serializerChanges(JsonGenerator writer, FuncSessionItemIncrement increment, ICefSessionItem cefSessionItem) {
        String temp = ((BEFuncSessionBase)cefSessionItem).getBEManager().serializeChanges(increment.getChangeDetails());
        SerializerUtils.writePropertyName(writer,"changeDetails");
        writer.writeRawValue(temp);
    }
}
