/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.buffer;

import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.cef.api.dataType.base.IAccessorCreator;
import com.inspur.edp.cef.core.buffer.base.BufferManager;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import javax.ws.rs.NotSupportedException;

//
//     *  0  originalAccessor
//     *  1  transactionAccessor
//     *  2  currentAccessor
//
public class BEBufferManager extends BufferManager
{
	public BEBufferManager(FuncSession session, String entitType, IAccessorCreator accessorCreator)
	{
		super(session, entitType, accessorCreator);
	}


		//region InitBuffer
	public IAccessor createCurrentBuffer(IEntityData data) {return initBufferByLevel(data, 2, false);}

	public IEntityData initTransactionBuffer(String id) {return  (IEntityData)initBufferByBaseLevel(id, 1, true);}
	public IEntityData initCurrentBuffer(String id) {return  (IEntityData)initBufferByBaseLevel(id, 2, false);}

		//endregion


		//region getData
	public IEntityData getCurrentData(String dataId){return  (IEntityData)getBuffer(dataId, 2);}

	public IEntityData getTransactionData(String dataId) {return (IEntityData)getBuffer(dataId, 1);}

	public IEntityData getOriginalData(String dataId) {return (IEntityData)getBuffer(dataId, 0);}

		//endregion


		//region Accept/Reject
	public final IAccessor acceptTemplateCurrentChange(String id, IChangeDetail change)
	{
		return acceptChange(id, 2, change);
	}

	public final IAccessor acceptCurrent(String id, IChangeDetail currentChange)
	{
		return accept(id, 2, currentChange);
	}

	public final IAccessor rejectCurrent(String id, IChangeDetail currentChange)
	{
		return reject(id, 2, currentChange);
	}

	public final IAccessor acceptTransaction(String id, IChangeDetail tranChange)
	{
		return accept(id, 1, tranChange);
	}

	public final IAccessor rejectTransaction(String id, IChangeDetail tranChange)
	{
		return reject(id, 1, tranChange);
	}

//	public final boolean hasData(String id) {
//		throw new NotSupportedException();
//	}
	//endregion
}
