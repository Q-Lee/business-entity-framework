/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.be;

import com.inspur.edp.bef.core.session.transaction.IBefTransactionItem;
import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.changeset.IChangesetManager;
import com.inspur.edp.cef.api.manager.IBufferManager;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.util.List;

public interface IBeBufferChangeManager
    extends IChangesetManager, IBufferManager, IBefTransactionItem {
  IAccessor acceptCurrent(String id);

  void acceptCurrentTemplateChange(String id);

  void acceptListenerChange(String ID);

  IAccessor acceptTemplateCurrentChange(String id, IChangeDetail change);

  IAccessor acceptTransaction(String id);

  void acceptTransactionChange(String id);

  void appendCurrentTemplateChange(IChangeDetail change);

  IAccessor createCurrentBuffer(IEntityData data);

  IChangeDetail getCurrentChange(String id);

  IEntityData getCurrentData(String dataId);

  IChangeDetail getCurrentTemplateChange(String id);

  IChangeDetail getListenerChange(String id);

  IEntityData getOriginalData(String dataId);

  IChangeDetail getTransactionChange(String id);

  IEntityData getTransactionData(String dataId);

  IEntityData initCurrentBuffer(String id);

  void initCurrentTemplateChange(IChangeDetail change);

  void initListenerChange(IChangeDetail change);

  IEntityData initTransactionBuffer(String id);

  IAccessor rejectCurrent(String id);

  void rejectCurrentChange(String id);

  void rejectCurrentTemplateChange(String id);

  void rejectTransaction(
      String id, RefObject<IEntityData> current, RefObject<IEntityData> transaction);

  void clearChange();

//  boolean isEmpty(String id);

  void notifyReset();

//  void recover(FuncSessionItemIncrement changes);
}
