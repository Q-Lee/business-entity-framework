/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.determination;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.cef.core.determination.EntityDeterminationExecutor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IEntityRTDtmAssembler;

public class BefOnCancelDtmExecutor extends EntityDeterminationExecutor {
  public BefOnCancelDtmExecutor(IEntityRTDtmAssembler assembler, IBENodeEntityContext entityCtx) {
    super(assembler, entityCtx);
  }

  protected IBENodeEntityContext innerGetEntityContext() {
    return (IBENodeEntityContext) super.getEntityContext();
  }

  @Override
  public void execute() {
    setChange(getChangeset());
    if (getChange() == null) {
      return;
    }

    setContext(GetContext());

    executeBelongingDeterminations();
    executeDeterminations();
    executeChildDeterminations();
  }

  @Override
  protected IChangeDetail getChangeset() {
    return innerGetEntityContext().getTransactionalChange();
  }
}
