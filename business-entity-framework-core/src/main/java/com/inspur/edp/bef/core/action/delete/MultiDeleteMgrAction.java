/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.delete;

import com.inspur.edp.bef.api.action.VoidActionResult;
import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.BefLockType;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.action.base.ActionUtil;
import com.inspur.edp.bef.core.be.BEManagerContext;
import com.inspur.edp.bef.core.lock.LockUtils;
import com.inspur.edp.bef.core.scope.BefScope;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.bef.spi.entity.builtinimpls.BefModelResInfoImpl;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class MultiDeleteMgrAction extends AbstractManagerAction<VoidActionResult>
{
	private java.util.List<String> dataIds;

	public MultiDeleteMgrAction(IBEManagerContext managerContext, java.util.List<String> dataIds) {
		super(managerContext);
		ActionUtil.requireNonNull(dataIds, "dataIds");
		this.dataIds = dataIds;
	}

	@Override
	public final void execute()
	{
		getBEManagerContext().checkAuthority("Delete");
//		AuthorityUtil.checkAuthority("Delete");
//		FuncSessionManager.getCurrentSession().getBefContext().setCurrentOperationType ("Delete");
		List<IBusinessEntity> beList = getBEManagerContext().getEntities(dataIds);
		BefScope scope = new BefScope(((BEManagerContext)getBEManagerContext()).getSession());
		RetrieveParam retrievePar = new RetrieveParam();
		retrievePar.setNeedLock(true);
		scope.beginScope();
		try
		{
			for (IBusinessEntity be : beList)
			{
				be.retrieveWithScope(retrievePar);
			}
			scope.setComplete();
		}
		catch (java.lang.Exception e)
		{
			scope.setAbort();
			throw e;
		}
		checkForLockFail(beList);

		beList.forEach(be -> be.delete());
	}

	private void checkForLockFail(List<IBusinessEntity> beList) {
		if(!LockUtils.needLock(getBEManagerContext().getModelResInfo()))
			return;
		List<IBusinessEntity> lockFailedBEs = beList.stream().filter(be -> !be.getBEContext().isLocked()).collect(
				Collectors.toList());
		if (!lockFailedBEs.isEmpty())
		{
			LockUtils.throwLockFailed(lockFailedBEs.stream().map(item -> item.getID()).collect(Collectors.toList()), null);
		}
	}

	@Override
	protected final IMgrActionAssembler getMgrAssembler() {
		return getMgrActionAssemblerFactory().getMultiDeleteMgrActionAssembler(getBEManagerContext());
	}
}
