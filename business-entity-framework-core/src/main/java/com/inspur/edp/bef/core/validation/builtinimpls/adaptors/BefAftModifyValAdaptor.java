/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.validation.builtinimpls.adaptors;

import com.inspur.edp.bef.api.action.determination.IDeterminationContext;
import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.core.validation.ValidationContext;
import com.inspur.edp.bef.spi.action.determination.IDeterminationExecutor;
import com.inspur.edp.bef.spi.action.validation.IValidationExecutor;
import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.core.determination.builtinimpls.dtmadaptors.AbstractEntityAfterModifyDtmAdaptor;
import com.inspur.edp.cef.core.validation.builtinimpls.CefAfterModifyValAssembler;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.ChildElementsTuple;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import java.util.List;

public class BefAftModifyValAdaptor extends BefBaseValAdaptor {
  public BefAftModifyValAdaptor(Class type, List<ChangeType> changeTypes) {
    super(type, changeTypes);
  }

  public BefAftModifyValAdaptor(Class type, List<ChangeType> changeTypes, List<String> elements) {
    super(type, changeTypes, elements);
  }

  public BefAftModifyValAdaptor(Class type, List<ChangeType> changeTypes, List<String> elements,
      List<ChildElementsTuple> childElements) {
    super(type, changeTypes, elements, childElements);
  }
}
