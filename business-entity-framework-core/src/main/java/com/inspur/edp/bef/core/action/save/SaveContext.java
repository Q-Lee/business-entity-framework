/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.save;

import java.util.ArrayList;
import java.util.List;

public class SaveContext {
  private List<SaveCompensater> compensatings = new ArrayList();

  public List<SaveCompensater> getCompensaters() {
    return compensatings;
  }

  public void addCompensater(SaveCompensater compensater) {
    this.compensatings.add(compensater);
  }

}
