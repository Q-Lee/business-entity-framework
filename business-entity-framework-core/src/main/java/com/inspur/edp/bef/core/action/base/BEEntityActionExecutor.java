/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.base;

import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.core.be.BEContext;
import com.inspur.edp.bef.core.be.BENodeEntityContext;
import com.inspur.edp.bef.core.be.CoreBEContext;
import com.inspur.edp.bef.core.session.BEFuncSessionBase;
import com.inspur.edp.bef.core.session.FuncSession;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.spi.action.AbstractAction;
import com.inspur.edp.cef.core.entityaction.EntityActionExecutor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import io.iec.edp.caf.boot.context.CAFContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BEEntityActionExecutor<T> extends EntityActionExecutor<T> {

  private BEFuncSessionBase sessionItem;
  private String beType;
  private String sessionId;

  public BEEntityActionExecutor(IBENodeEntityContext beCtx) {
    super.setContext(beCtx);
    beType = ActionUtil.getRootBEContext(beCtx).getBizEntity().getBEType();
//    FuncSession currentSession = FuncSessionManager.getCurrentSession();
//    sessionId = currentSession.getSessionId();
    sessionItem = beCtx instanceof BENodeEntityContext ? ((BENodeEntityContext)beCtx).getSessionItem() : ((BEContext)beCtx).getSessionItem(); // currentSession.getFuncSessionItem(beType);
  }

  public IBENodeEntityContext getBEContext() {
    return (IBENodeEntityContext) super.getContext();
  }
//
//  public void setContext(IBENodeEntityContext value) {
//    super.setContext(value);
//  }

  public AbstractAction<T> getBEAction() {
    return (AbstractAction<T>) super.getAction();
  }

  public void setAction(AbstractAction<T> value) {
    super.setAction(value);
  }

  @Override
  protected void onBeforeExecute() {
    if (sessionItem.getActionMark() == null) {
      sessionItem.setActionMark(this);
    }
    super.onBeforeExecute();
  }

  @Override
  protected void onAfterExecute() {
    super.onAfterExecute();
    CoreBEContext rootBEContext = ActionUtil.getRootBEContext(getBEContext());
    rootBEContext.mergeIntoInnerChange();
    rootBEContext.acceptListenerChange();
    if (rootBEContext == getBEContext()) {
      IChangeDetail change = rootBEContext.getCurrentChange();
      if (change != null) {
        com.inspur.edp.bef.core.lock.LockUtils.checkLock(rootBEContext);
      }
    }
    // Context.acceptTempChange();
  }

  @Override
  protected void determinateAndValidate() {
    if (!isOutmost()) {
      return;
    }
    super.determinateAndValidate();
    CoreBEContext rootBEContext = ActionUtil.getRootBEContext(getBEContext());
    rootBEContext.mergeIntoInnerChange();
    rootBEContext.acceptListenerChange();
    rootBEContext.acceptTempChange();
  }

  @Override
  protected void onFinally() {
//    ActionUtil.checkSessionChanged(sessionId,beType,ActionUtil.CATEGORY_ENTITY_ACTION, getBEAction());
    if (isOutmost()) {
      sessionItem.setActionMark(null);
    }
  }

  private boolean isOutmost() {
    return this.equals(sessionItem.getActionMark());
  }

  @Override
  protected boolean handleException(RuntimeException e) {
    if (isOutmost()) {
      CoreBEContext rootBEContext = ActionUtil.getRootBEContext(getBEContext());
      rootBEContext.rejectChanges();
    }
    return super.handleException(e);
  }
}
