/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.save.saveEvent;

import com.inspur.edp.bef.api.be.BEInfo;
import com.inspur.edp.bef.api.event.save.AfterSaveEventArgs;
import com.inspur.edp.bef.api.event.save.BeforeSaveEventArgs;
import com.inspur.edp.bef.core.action.save.saveEvent.SaveEventType;
import com.inspur.edp.bef.spi.event.saveactionevent.IBefSaveEventListener;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import io.iec.edp.caf.commons.event.EventManager;
import io.iec.edp.caf.commons.event.IEventListener;

public class SaveEventManager extends EventManager
{
//    public static SaveEventManager getInstance()
//    {
//        return new SaveEventManager();
//    }

    @Override
    public String getEventManagerName(){
        return  "BefSaveEventManager";
    }
    @Override
    public void addListener(IEventListener listener)
    {
        if(!(listener instanceof IBefSaveEventListener))
            throw new RuntimeException("指定的监听者没有实现IBefSaveEventListener接口！");
        IBefSaveEventListener saveEventListener = (IBefSaveEventListener)listener;
        this.addEventHandler(SaveEventType.BeforeSave,saveEventListener,"beforeSave");
        this.addEventHandler(SaveEventType.AfterSave, saveEventListener,"afterSave");
    }
    @Override
    public void removeListener(IEventListener listener)
    {
        if(!(listener instanceof IBefSaveEventListener))
            throw new RuntimeException("指定的监听者没有实现IBefSaveEventListener接口！");
        IBefSaveEventListener saveEventListener = (IBefSaveEventListener)listener;
        this.removeEventHandler(SaveEventType.BeforeSave,saveEventListener,"beforeSave");
        this.removeEventHandler(SaveEventType.AfterSave, saveEventListener,"afterSave");
    }

    public final void fireBeforeSave(BEInfo beInfo, String funcSessionID, java.util.ArrayList<String> dataIds,
                                     java.util.ArrayList<IChangeDetail> changes)
    {
        this.fire(SaveEventType.BeforeSave, new BeforeSaveEventArgs(beInfo, dataIds, changes, funcSessionID));
    }
    public final void fireAfterSave(BEInfo beInfo, String funcSessionID, java.util.ArrayList<String> dataIds,
                                    java.util.ArrayList<IChangeDetail> changes)
    {
        this.fire(SaveEventType.AfterSave, new AfterSaveEventArgs(beInfo, dataIds, changes, funcSessionID));
    }

    @Override
    public boolean isHandlerListener(IEventListener iEventListener) {
        return iEventListener instanceof IBefSaveEventListener;
    }
}
