/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.core.action.retrievedefault;

import com.inspur.edp.bef.api.action.assembler.IMgrActionAssembler;
import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.bef.core.DotNetToJavaStringHelper;
import com.inspur.edp.bef.core.action.AuthorityUtil;
import com.inspur.edp.bef.core.session.FuncSessionManager;
import com.inspur.edp.bef.spi.action.AbstractManagerAction;
import com.inspur.edp.bef.spi.entity.builtinimpls.BefEntityResInfoImpl;
import com.inspur.edp.bef.spi.entity.columnconfig.ColumnGenerateConfigLoader;
import com.inspur.edp.bef.spi.entity.columnconfig.ColumnGenerateInfo;
import com.inspur.edp.cef.entity.changeset.ModifyChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.entity.resourceInfo.EntityResInfo;
import com.inspur.edp.udt.entity.IUdtData;
import java.util.Map.Entry;
import java.util.UUID;

public class RetrieveDefaultManagerAction extends AbstractManagerAction<IEntityData>
{

		//region Consturctor
	private boolean useID;
	private String dataID;
	private java.util.Map<String, Object> defaultValues;
	public RetrieveDefaultManagerAction(IBEManagerContext managerContext, boolean useID, String dataID, java.util.Map<String, Object> defaultValues)
	{
		super(managerContext);
		this.useID = useID;
		this.dataID = dataID;
		this.defaultValues = defaultValues;
	}


		//endregion


		//region Override

	private String getID() {
		if (DotNetToJavaStringHelper.isNullOrEmpty(dataID) == false) {
			return dataID;
		}
		 return getColumnId();
	}

	@Override
	public final void execute()
	{
		getBEManagerContext().checkAuthority("RetrieveDefault");
//		AuthorityUtil.checkAuthority("RetrieveDefault");
//		FuncSessionManager.getCurrentSession().getBefContext().setCurrentOperationType("RetrieveDefault");
	   //var com.inspur.edp.bef.core.be = (IBusinessEntity)BEManagerContext.Manager.createEntity(GetID());
		IBusinessEntity be = getBEManagerContext().getEntity(getID());
		be.retrieveDefault(defaultValues);
		modifyByDefaultValue(be);
		setResult(be.getBEContext().getCurrentData());
	}

	private void modifyByDefaultValue(IBusinessEntity be) {
		if(defaultValues == null || defaultValues.isEmpty()) {
			return;
		}
		ModifyChangeDetail change = new ModifyChangeDetail(be.getID());
		for (Entry<String, Object> pair : defaultValues.entrySet()) {
			if(pair.getValue() != null && pair.getValue() instanceof IUdtData) {
				continue;
			}
			change.getPropertyChanges().put(pair.getKey(), pair.getValue());
		}
		if(!change.getPropertyChanges().isEmpty()) {
			be.modify(change);
		}
	}

	@Override
	protected final IMgrActionAssembler getMgrAssembler()
	{
		return getMgrActionAssemblerFactory().getRetrieveDefaultMgrActionAssembler(getBEManagerContext());
	}

	private EntityResInfo getEntityResInfo(){
		return  this.getBEManagerContext().getBEManager().getRootEntityResInfo();
	}

	private String getColumnId(){

		EntityResInfo resInfo=this.getEntityResInfo();
		if(!(resInfo instanceof BefEntityResInfoImpl))
			return UUID.randomUUID().toString();
		ColumnGenerateInfo columnGenerateInfo=((BefEntityResInfoImpl)resInfo).getColumnGenerateInfo();
		return ColumnGenerateConfigLoader.getId(columnGenerateInfo);
	}
		//endregion
}
