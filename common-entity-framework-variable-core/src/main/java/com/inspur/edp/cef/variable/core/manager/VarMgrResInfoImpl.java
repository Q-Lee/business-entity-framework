package com.inspur.edp.cef.variable.core.manager;

import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefValueObjModelResInfo;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefValueObjResInfo;

public abstract class VarMgrResInfoImpl extends CefValueObjModelResInfo {

  public VarMgrResInfoImpl(String currentSu, String metadataId, String rootNodeCode,
      String displayKey) {
    super(currentSu, metadataId, rootNodeCode, displayKey);
  }
}

