package com.inspur.edp.cef.variable.core.variable;

import com.inspur.edp.cef.api.dataType.action.IDataTypeActionExecutor;
import com.inspur.edp.cef.api.dataType.base.ICefDataType;
import com.inspur.edp.cef.api.dataType.valueObj.ICefValueObject;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.cef.variable.api.variable.IVariableContext;

import java.net.UnknownServiceException;

public class VariableContext implements IVariableContext
{
	public VariableContext()
	{
	}

	private IValueObjData privateData;
	public final IValueObjData getData()
	{
		return privateData;
	}

	public void setData(ICefData iCefData) {
		setData((IValueObjData)iCefData);
	}

	public final void setData(IValueObjData value)
	{
		privateData = value;
	}
	private ICefValueObject privateDataType;
	public final ICefValueObject getDataType()
	{
		return privateDataType;
	}

	public void setDataType(ICefDataType iCefDataType) {
		privateDataType= (ICefValueObject) iCefDataType;
	}

	public <T> IDataTypeActionExecutor<T> getActionExecutor() {
		return null;
	}

	public final void setDataType(ICefValueObject value)
	{
		privateDataType = value;
	}


	/** 
	 本次Modify触发的联动计算产生的内部变更, 不包含Modify传入的变更
	 
	*/
	private IChangeDetail privateInnerChange;
	public final IChangeDetail getInnerChange()
	{
		return privateInnerChange;
	}
	public final void setInnerChange(IChangeDetail value)
	{
		privateInnerChange = value;
	}

	private <T> IDataTypeActionExecutor<T> GetActionExecutor()
	{
		throw new UnsupportedOperationException();
	}
}