/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.udt.core.Manager;

import com.inspur.edp.cef.api.manager.ICefDataTypeManager;
import com.inspur.edp.cef.api.manager.ICefValueObjManager;
import com.inspur.edp.cef.api.manager.action.IMgrActionExecutor;
import com.inspur.edp.cef.spi.manager.MgrActionExecutor;
import com.inspur.edp.udt.api.Manager.IUdtManager;
import com.inspur.edp.udt.api.Manager.IUdtMgrContext;

public final class UdtMgrContext implements IUdtMgrContext
{
	public UdtMgrContext()
	{
	}

	private IUdtManager udtManager;

	public IUdtManager getUdtManager()
	{
		return udtManager;
	}
	public void setUdtManager(IUdtManager value)
	{
		udtManager=value;
	}

	public ICefValueObjManager getManager() {return getUdtManager();}

	public void setManager(ICefDataTypeManager value)
	{
		setUdtManager((IUdtManager)value);
	}

	public <T> IMgrActionExecutor<T> getActionExecutor()
	{
		return new MgrActionExecutor<T>();
	}
}
