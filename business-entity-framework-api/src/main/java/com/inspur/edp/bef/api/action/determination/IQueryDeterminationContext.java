/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.action.determination;

import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.variable.api.data.IVariableData;

/** 查询前联动规则上下文接口 */
public interface IQueryDeterminationContext {
  /**
   * 自定义过滤条件
   * <p>可以通过修改过滤条件来控制查询行为。在此处设置的过滤条件具有最高优先级。
   * <p>注意，非业务逻辑固有过滤条件不应在这里设置。
   */
  EntityFilter getFilter();

  void setFilter(EntityFilter value);

  /** 获取自定义变量*/
  IVariableData getVariables();


  //region i18n
  /**
   * 翻译后的实体名称
   *
   * @return
   */
  String getEntityI18nName(String nodeCode);
  /**
   * 翻译后的属性名称
   *
   * @param labelID
   * @return
   */
  String getPropertyI18nName(String nodeCode, String labelID);
  /**
   * 翻译后的关联带出字段名称
   *
   * @param labelID
   * @param refLabelID
   * @return
   */
  String getRefPropertyI18nName(String nodeCode, String labelID, String refLabelID);
  /**
   * 翻译后的枚举显示值
   *
   * @param labelID
   * @param enumKey
   * @return
   */
  String getEnumValueI18nDisplayName(String nodeCode, String labelID, String enumKey);
  /**
   * 翻译后的唯一性约束提示信息
   *
   * @param conCode
   * @return
   */
  String getUniqueConstraintMessage(String nodeCode, String conCode);

  //endregion
}
