/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.lcp;

import com.inspur.edp.cef.api.message.IBizMessage;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

/**
 lcp一次请求执行结束后的相关信息
 
*/
public interface IResponseContext
{
	/** 
	 一次请求执行过程中产生的内部变更
	 
	*/
	java.util.HashMap<String, IChangeDetail> getInnerChangeset();

	/** 
	 一次请求执行过程中产生的变量内部变更
	 
	*/
	IChangeDetail getVariableInnerChange();


	/** 
	 一次请求执行过程中产生的消息
	 
	*/
	java.util.List<IBizMessage> getMessages();

	void clear();

	/** 
	 增加消息
	 
	 @param msgList 消息列表
	//void AddMessage(IList<IBizMessage> msgList);
	*/

	/** 
	 增加消息
	 
	 @param msgList 消息列表
	//void AddMessage(params IBizMessage[] msgList);
	*/
}
