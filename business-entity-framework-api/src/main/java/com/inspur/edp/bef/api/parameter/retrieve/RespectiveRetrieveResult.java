/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.parameter.retrieve;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.util.Arrays;
import java.util.Optional;

/** 单行数据检索时返回的结果类 */
public class RespectiveRetrieveResult {
  /** 数据检索时获取到的实体数据。 */
  private IEntityData privateData;
  /** 数据检索时获取到的实体数据。
   * <p>
   * 如果数据不存在，此方法将返回{@code null}。
   * <p>
   * 如果数据在持久化中存在，但是在当前缓存中已被标记为删除（即调用过{@link com.inspur.edp.bef.api.lcp.IStandardLcp#delete(String)}或其他删除方法），那么此数据被认为不存在，此方法返回值为{@code null}。
   * <p>
   * 如果是新增的数据，即使未执行过保存，那么此数据也被认为存在，此方法返回值不为{@code null}。
   * */
  public final IEntityData getData() {
    return privateData;
  }

  /**
   * 获取检索结果的{@link Optional}形式。
   * 相比{@link #getData()}方法，此方法返回值永不为null，且更便于进行函数式风格的编程。
   */
  @JsonIgnore
  public <T extends IEntityData> Optional<T> getOptionalData(){
    return Optional.ofNullable((T)getData());
  }

  public final void setData(IEntityData value) {
    privateData = value;
  }

  /** 加锁是否成功 */
  private boolean privateLockFailed;

  /**
   * 如果使用加锁的方式进行检索，此方法返回值代表加锁是否成功。
   * 如果未使用加锁的方法，则此方法返回值一定为false。
   * @return 加锁是否成功
   *
   * @see com.inspur.edp.bef.api.lcp.IStandardLcp#retrieve(String, RetrieveParam)
   * @see RetrieveParam#getNeedLock()
   */
  public final boolean getLockFailed() {
    return privateLockFailed;
  }

  public final void setLockFailed(boolean value) {
    privateLockFailed = value;
  }
}
