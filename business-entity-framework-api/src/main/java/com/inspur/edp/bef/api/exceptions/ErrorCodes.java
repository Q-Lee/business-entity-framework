/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.exceptions;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public final class ErrorCodes {
  //
  public static final String UnknownChangeType = "GSP_ADP_BEF_1001";
  public static final String ValidationNotPass = "GSP_ADP_BEF_1002";

  // 业务逻辑
  public static final String RepeatCreatingSession = "GSP_ADP_BEF_2000";
  public static final String EditingLocked = "GSP_ADP_BEF_2001";
  public static final String AcceptChanges = "GSP_ADP_BEF_2002";
  public static final String InvalidRecreation = "GSP_ADP_BEF_2003";
  public static final String DeletingNonExistence = "GSP_ADP_BEF_2004";
  public static final String ModifyingNonExistence = DeletingNonExistence;
  public static final String AcceptingNochangeToTransaction = "GSP_ADP_BEF_2005";
  public static final String UnlockingChanged = "GSP_ADP_BEF_2006";
  public static final String UnexpectedLockResult = "GSP_ADP_BEF_2007";
  public static final String CirclDtmFound = "GSP_ADP_BEF_2008";
  public static final String InvalidSaveOperation = "GSP_ADP_BEF_2009";
  public static final String SessionNotExists = "GSP_ADP_BEF_2010";
  public static final String Scope_BizContextDestroyFailed = "Cef2011";
  public static final String Scope_BizContextNotFound = "Cef2012";
  public static final String SessionNotValid = "GSP_ADP_BEF_2013";
  public static final String DataVersionMismatch = "GSP_ADP_BEF_2014";
  public static final String SessionTimeout = "GSP_ADP_BEF_2015";

  // 权限
  public static final String DataPermissionDenied = "GSP_ADP_BEF_4001";
  public static final String FuncPermissionDenied = "GSP_ADP_BEF_4002";

  // 操作执行
  public static final String ExcuteMgrAction = "GSP_ADP_BEF_0501";

  public static final String NoScope = "GSP_ADP_BEF_0601";

  //Tcc
  public static final String UnsupportedInTcc = "GSP_ADP_BEF_0701";
}
