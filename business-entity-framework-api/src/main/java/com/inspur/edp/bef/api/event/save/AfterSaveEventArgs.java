/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.event.save;

import com.inspur.edp.bef.api.be.BEInfo;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

/** 保存后扩展事件参数 继承自 */
public class AfterSaveEventArgs extends SaveEventArgs {
  public AfterSaveEventArgs(
          BEInfo beInfo,
          java.util.ArrayList<String> dataIDs,
          java.util.ArrayList<IChangeDetail> changes,
          String funcSessionID) {
    super(beInfo, dataIDs, changes, funcSessionID);
  }

  private AfterSaveEventArgs()
  {}

  public AfterSaveEventArgs createNewArgs()
  {
    AfterSaveEventArgs beforeSaveEventArgs=new AfterSaveEventArgs();
    copyToNewArgs(beforeSaveEventArgs);
//    beforeSaveEventArgs.getBeInfo().setRootEntityCode(newBeInfo.getRootEntityCode());
//    beforeSaveEventArgs.getBeInfo().setBEID(newBeInfo.getBEID());
//    beforeSaveEventArgs.getBeInfo().setBEType(newBeInfo.getBEType());
    return  beforeSaveEventArgs;
  }
}
