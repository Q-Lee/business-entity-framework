/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.parameter.retrieve;

import com.inspur.edp.cef.entity.entity.IEntityData;
import java.util.ArrayList;
import java.util.List;

/** 批量数据检索时返回的结果类 */
public class RetrieveResult {
  private java.util.HashMap<String, IEntityData> dataDict;

  /**
   * 数据检索时获取到的实体数据字典，键为实体数据唯一标识，值为业务实体主表数据。
   * <p>如需获取子表数据，可以使用{@link com.inspur.edp.bef.api.be.IBEService#retrieveChild(List, List, ArrayList, RetrieveParam)}，
   * 或参考{@link IEntityData#getChilds()}，前者只能获取特定某一条主表下的子表数据。
   */
  public final java.util.HashMap<String, IEntityData> getDatas() {
    if (dataDict == null) {
      dataDict = new java.util.LinkedHashMap<String, IEntityData>();
    }
    return dataDict;
  }

  public final void setDatas(java.util.HashMap<String, IEntityData> value) {
    dataDict = value;
  }

  private java.util.Collection<String> lockFailedIds;

  /** 记录数据检索时，加锁失败的实体数据的唯一标识的集合。  */
  public final java.util.Collection<String> getLockFailedIds() {
    if (lockFailedIds == null) {
      lockFailedIds = new java.util.HashSet<String>();
    }
    return lockFailedIds;
  }

  public final void setLockFailedIds(java.util.Collection<String> value) {
    lockFailedIds = value;
  }
}
