/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.parameter.save;

/**
 * 调用{@link com.inspur.edp.bef.api.lcp.IStandardLcp#save(SaveParameter)}方法时的参数。
 * 使用参数可以控制一些保存时的行为。大部分场景下使用{@link #getDefault()}方法得到一个默认参数即可。
 */
public final class SaveParameter {

  /**
   * 默认情况下，保存成功后清空锁。
	 * @see #getClearLock()
   */
  public static SaveParameter getDefault() {
    SaveParameter rez = new SaveParameter();
    rez.setClearLock(true);
    return rez;
  }

  /**
   * 保存成功后是否清空锁
   */
  private boolean privateClearLock;

  /**
   * 获取保存成功后是否清空数据锁。
   * 关于锁的详细说明请访问<a href="https://open.inspuronline.com/iGIX/#/document/mddoc/docs-gsp-cloud-ds%2Fdev-guide-beta%2Fspecial-subject%2FBE-related%2FBE%E6%95%B0%E6%8D%AE%E9%94%81%E8%AF%B4%E6%98%8E.md”>这里</a>查看。
   * <p>由于大部分业务场景的最后一步都是保存，所以保存后通常应释放数据锁。
	 * 但是如果希望先进行保存，然后对之前修改的数据继续进行修改，那么为了防止保存和继续修改的间隙，数据被其他人抢先加锁导致当前业务无法继续进行，
	 * 则可以设置为保存成功后不清空数据锁。
   * @return 是否清空锁
   */
  public boolean getClearLock() {
    return privateClearLock;
  }

	/**
	 * 设置保存成功后是否清空锁
	 * @param value 是否清空锁
	 *
	 * @see #getClearLock()
	 */
  public void setClearLock(boolean value) {
    privateClearLock = value;
  }
}
