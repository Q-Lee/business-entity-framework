/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.api.exceptions;

import com.inspur.edp.bef.entity.exception.BefExceptionBase;
import com.inspur.edp.bef.entity.exception.ExceptionLevel;

// bdf引擎内异常, 不显示给用户, message可以不国际化
// 此异常与GSPException/Exception都应该在边界处捕获后禁用session
public class BefEngineException extends BefExceptionBase {
  private static final String resourceFile = "";

  public BefEngineException(
      String exceptionCode, String exceptionMessage, RuntimeException innerException) {
    super(exceptionCode, exceptionMessage, innerException, ExceptionLevel.Error, false);
  }


  public BefEngineException(
      String exceptionCode,
      RuntimeException innerException,
      ExceptionLevel level,
      boolean isBizException) {
    super(exceptionCode, resourceFile, innerException, null, level, isBizException);
  }

  //	protected BefEngineException(SerializationInfo info, StreamingContext context)
  //	{
  //		super(info, context);
  //	}
}
