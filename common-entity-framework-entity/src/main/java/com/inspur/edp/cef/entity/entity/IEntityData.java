/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.entity;

import com.inspur.edp.caf.cef.rt.api.annotation.CommonDataAccessorHandler;

/**
 * 实体数据
 * <p>目前视图模型、业务实体的实体数据均实现此接口，业务字段数据不实现
 */
@CommonDataAccessorHandler(accessorType = "IEntityData")
public interface IEntityData extends ICefData, IKey {

	/**
	 * 获取当前数据的直接下级子表数据
	 * <p>对于生成型和解析型，如下代码可以获取一个编号为X的子表，并对子表行进行遍历:
	 * <blockquote><pre>
	 *     IEntityData data = ...;
	 *     IEntityDataCollection childDatas = data.getChilds().get("X");
	 *     for(IEntityData childData : childDatas) {
	 *       //注意在此循环中禁止对childDatas进行增、删操作
	 *     }
	 * </pre></blockquote>
	 * <p>对于生成型，可强转为生成类型后通过getXs()方法获取子表集合。
	 *
	 * @return 下级子表数据，key为子表编号
	 */
	java.util.HashMap<String, IEntityDataCollection> getChilds();

	ICefData createChild(String childCode);

	void mergeChildData(String nodeCode, java.util.ArrayList<IEntityData> childData);

}
