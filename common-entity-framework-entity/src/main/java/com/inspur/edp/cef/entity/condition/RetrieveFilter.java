/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

import java.util.*;

public class RetrieveFilter {
    private boolean onlyRetrieveSomeChilds = false;

    private boolean enableMultiLanguage = false;


    private List<String> retrieveChildNodeList = new ArrayList<>();

    public List<String> getRetrieveChildNodeList() {
        return retrieveChildNodeList;
    }

    private Map<String, EntityFilter> nodeFilters = new HashMap<>();

    public Map<String, EntityFilter> getNodeFilters() {
        return nodeFilters;
    }

    public boolean isOnlyRetrieveSomeChilds() {
        return onlyRetrieveSomeChilds;
    }

    public void setOnlyRetrieveSomeChilds(boolean onlyRetrieveSomeChilds) {
        this.onlyRetrieveSomeChilds = onlyRetrieveSomeChilds;
    }

    /**
     * retrieve返回结果中包含多语相关内容
     *
     * @return
     */
    public boolean isEnableMultiLanguage() {
        return enableMultiLanguage;
    }

    public void setEnableMultiLanguage(boolean enableMultiLanguage) {
        this.enableMultiLanguage = enableMultiLanguage;
    }

    private boolean canExcludedChilds=false;

    /**
     * 是否排除从从表及以下数据
     * @return
     */
    public boolean isCanExcludedChilds() {
        return canExcludedChilds;
    }

    public void setCanExcludedChilds(boolean canExcludedChilds) {
        this.canExcludedChilds = canExcludedChilds;
    }

}
