/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.accessor.dataType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.cef.entity.accessor.base.AccessorBase;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IValueObjData;

public abstract class ValueObjAccessor extends AccessorBase
{
	protected ValueObjAccessor()
	{
	}

	protected ValueObjAccessor(IValueObjData inner)
	{
		super(inner);
	}


	//[JsonIgnore]
	@JsonIgnore
	private String privatePropName;
	@JsonIgnore
	public final String getPropName()
	{
		return privatePropName;
	}
	public final void setPropName(String value)
	{
		privatePropName = value;
	}


	@JsonIgnore
	private IAccessor privateBelongBuffer;
	@JsonIgnore
	public final IAccessor getBelongBuffer()
	{
		return privateBelongBuffer;
	}
	public final void setBelongBuffer(IAccessor value)
	{
		privateBelongBuffer = value;
	}



	@Override
  public final IAccessor getRoot()
	{
        IAccessor belongBuffer = getBelongBuffer();
		if(belongBuffer!=null)
			return belongBuffer.getRoot();
		else
			return this;
	}

	protected final <T> T readAssociation(Object value)
	{
		return  null;

	}

	@Override
	public ICefData copy() {
		ValueObjAccessor rez = (ValueObjAccessor)super.copy();
		rez.setPropName(getPropName());
		return rez;
	}
}
