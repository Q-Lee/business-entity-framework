/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.entity;

public final class EntityDataUtils {
    public static Object getValue(IEntityData data, String propertyName) {
        return data.getValue(propertyName);
    }

    public static void setValue(IEntityData data, String propertyName, Object value) {
        data.setValue(propertyName, value);
    }

    public static IEntityDataCollection getChildDatas(IEntityData data, String childCode) {
        return data.getChilds().get(childCode);
    }
}
