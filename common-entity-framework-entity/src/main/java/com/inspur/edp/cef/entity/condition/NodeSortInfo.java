/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.condition;

public class NodeSortInfo
{
	private String privateNodeCode;
	public final String getNodeCode()
	{
		return privateNodeCode;
	}
	public final void setNodeCode(String value)
	{
		privateNodeCode = value;
	}
	private java.util.ArrayList<SortCondition> privateSortConditions;
	public final java.util.ArrayList<SortCondition> getSortConditions()
	{
		return privateSortConditions;
	}
	public final void setSortConditions(java.util.ArrayList<SortCondition> value)
	{
		privateSortConditions = value;
	}

}
