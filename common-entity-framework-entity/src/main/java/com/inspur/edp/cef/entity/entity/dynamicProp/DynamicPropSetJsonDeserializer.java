/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.entity.dynamicProp;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;

public class DynamicPropSetJsonDeserializer extends JsonDeserializer<IDynamicPropSet> {
  @Override
  public IDynamicPropSet deserialize(JsonParser jsonParser,
      DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
    IDynamicPropSet set =new DynamicPropSetImpl();
    jsonParser.nextToken();
    while (jsonParser.getCurrentToken()!= JsonToken.END_OBJECT)
    {
      String propName = jsonParser.getValueAsString();
      jsonParser.nextToken();
      Object value;
      if (jsonParser.getCurrentToken() == JsonToken.VALUE_STRING)
      {
        value = jsonParser.getValueAsString();
      }
      else
      {
        value = jsonParser.readValueAsTree();
      }
      set.setValue(propName, value);
      jsonParser.nextToken();
    }
    return set;
  }
}
