/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.changeset;

/** 
 变更集类型
 
*/
public enum ChangeType 
{
	/** 
	 新增实体数据时的变更集类型
	 
	*/
	Added(0),

	/** 
	 修改实体数据时的变更集类型
	 
	*/
	Modify(1),

	/** 
	 删除实体数据时的变更集类型
	 
	*/
	Deleted(2),

	AddedOrModify(3);

	private int intValue;
	private static java.util.HashMap<Integer, ChangeType> mappings;
	private synchronized static java.util.HashMap<Integer, ChangeType> getMappings()
	{
		if (mappings == null)
		{
			mappings = new java.util.HashMap<Integer, ChangeType>();
		}
		return mappings;
	}

	private ChangeType(int value)
	{
		intValue = value;
		ChangeType.getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static ChangeType forValue(int value)
	{
		return getMappings().get(value);
	}

	public static boolean isStringChangeType(String changeType) {
		for (ChangeType value : getMappings().values()) {
			if(value.toString().equals(changeType))
				return true;
		}
		return false;
	}
}
