/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.changeset;

import com.inspur.edp.cef.entity.entity.IValueObjData;

public class ValueObjModifyChangeDetail extends AbstractModifyChangeDetail
{
	public ValueObjModifyChangeDetail()
	{
	}

	private IValueObjData privateData;
	public final IValueObjData getData()
	{
		return privateData;
	}
	public final void setData(IValueObjData value)
	{
		privateData = value;
	}

	public 	IChangeDetail clone()
	{
		return InnerUtil.cloneValueModifyChange(this);

	}

	@Override
	public ValueObjModifyChangeDetail putItem(String key, Object value) {
		return (ValueObjModifyChangeDetail)super.putItem(key, value);
	}

	@Override
	public ValueObjModifyChangeDetail putItem(String key, String nestedKey, Object value) {
		return (ValueObjModifyChangeDetail)super.putItem(key, nestedKey, value);
	}

}
