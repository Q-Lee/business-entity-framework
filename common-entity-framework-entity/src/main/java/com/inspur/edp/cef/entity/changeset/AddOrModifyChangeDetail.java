/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.changeset;

import com.inspur.edp.cef.entity.entity.IKey;
import java.util.Objects;

public class AddOrModifyChangeDetail implements IChangeDetail, IKey {
  private ModifyChangeDetail changeDetail;

  public AddOrModifyChangeDetail(){

  }

  public AddOrModifyChangeDetail(ModifyChangeDetail change) {
    Objects.requireNonNull(change, "change");

    this.changeDetail = change;
  }

  public ModifyChangeDetail getModifyChange() {
    return changeDetail;
  }

  public void setModifyChange(ModifyChangeDetail change) {
    this.changeDetail = change;
  }
  @Override
  public ChangeType getChangeType() {
    return ChangeType.AddedOrModify;
  }

  @Override
  public String getDataID() {
    return getModifyChange() == null ? null : getModifyChange().getDataID();
  }

  @Override
  public void setDataID(String value) {
    if(getModifyChange() == null) {
      throw new IllegalStateException();
    }
    getModifyChange().setDataID(value);
  }

  @Override
  public IChangeDetail clone() {
    return new AddOrModifyChangeDetail(
        getModifyChange() != null ? (ModifyChangeDetail) getModifyChange().clone() : null);
  }

  @Override
  public String getID() {
    return getDataID();
  }

  @Override
  public void setID(String value) {
    setDataID(value);
  }
}
