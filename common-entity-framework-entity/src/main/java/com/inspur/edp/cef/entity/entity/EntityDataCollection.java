/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.entity;

import java.util.*;

public class EntityDataCollection  implements IEntityDataCollection
{
	private java.util.LinkedHashMap<String, IEntityData> dic = new java.util.LinkedHashMap<String, IEntityData>();

	public final IEntityData getItem(String id)
	{
		return dic.get(id);
	}

	public final void setItem(String id, IEntityData value)
	{
		dic.put(id, value);
	}

	public int getCount()
	{
return dic.size();
	}

	public boolean getIsReadOnly()
	{return false;}

	private   void innerAdd(IEntityData item)
	{
		/*
		Java迁移临时屏蔽特性
		DataValidator.checkForNullReference(item, "item");
		*/
		dic.put(item.getID(), item);
	}

	protected Collection<IEntityData> innerGetAll() {
		return dic.values();
	}

	public final void innerClear()
	{
		dic.clear();
	}

	public final boolean contains(String id)
	{
		return dic.containsKey(id);
	}

	public final boolean contains(IEntityData item)
	{
		return dic.containsValue(item);
	}

	public final void copyTo(IEntityData[] array, int arrayIndex)
	{
		java.util.Iterator<IEntityData> enumerator = iterator();
		for (int i = arrayIndex; i < getCount(); i++)
		{
			enumerator.hasNext();
			array[i] = enumerator.next();
		}
	}

	public final IEntityData tryGet(String id)
	{
		IEntityData result = dic.get(id);
		return result;
	}

	public boolean remove(String id) {
		dic.remove(id);
		return true;
	}


	public int size() {
		return dic.size();
	}


	public boolean isEmpty() {
		return dic.isEmpty();
	}


	public boolean contains(Object o) {
		return dic.containsValue(o);
	}


	public Iterator<IEntityData> iterator() {
		return dic.values().iterator();
	}


	public Object[] toArray() {
		return dic.values().toArray();
	}


	public <T> T[] toArray(T[] a) {
		//Java迁移临时屏蔽特性
			throw new UnsupportedOperationException();
	}


	public boolean add(IEntityData iEntityData) {
		dic.put(iEntityData.getID(), iEntityData);
		return true;
	}


	public boolean remove(Object o) {
		//Java迁移临时屏蔽特性
	return remove((String)o);
	}


	public boolean containsAll(Collection<?> c) {
		//Java迁移临时屏蔽特性
		throw new UnsupportedOperationException();
	}


	public boolean addAll(Collection<? extends IEntityData> c) {
		Objects.requireNonNull(c);
		c.stream().forEach(item -> add(item));
		return true;
	}


	public boolean removeAll(Collection<?> c) {
		//Java迁移临时屏蔽特性
		throw new UnsupportedOperationException();
	}


	public boolean retainAll(Collection<?> c) {
		//Java迁移临时屏蔽特性
		throw new UnsupportedOperationException();
	}


	public void clear() {
dic.clear();
	}
}
