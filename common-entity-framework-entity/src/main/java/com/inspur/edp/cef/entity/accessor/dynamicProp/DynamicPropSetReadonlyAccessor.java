/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.entity.accessor.dynamicProp;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inspur.edp.cef.entity.accessor.base.AccessorBase;
import com.inspur.edp.cef.entity.accessor.base.ReadonlyDataException;
import com.inspur.edp.cef.entity.accessor.dataType.ValueObjAccessor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.dynamicProp.DynamicPropSetJsonDeserializer;
import com.inspur.edp.cef.entity.entity.dynamicProp.DynamicPropSetJsonSerializer;
import com.inspur.edp.cef.entity.entity.dynamicProp.IDynamicPropSet;

@JsonDeserialize( using = DynamicPropSetJsonDeserializer.class)
@JsonSerialize(using = DynamicPropSetJsonSerializer.class)
public class DynamicPropSetReadonlyAccessor extends ValueObjAccessor implements IDynamicPropSet {
  public DynamicPropSetReadonlyAccessor() {}

  public DynamicPropSetReadonlyAccessor(IDynamicPropSet inner) {
    super(inner);
  }

  @Override
  public boolean getIsReadonly() {
    return true;
  }

  public IDynamicPropSet getInnerData() {
    return (IDynamicPropSet) super.getInnerData();
  }

  public Object getValue(String propName) {
    if (getInnerData() == null) {
      throw new RuntimeException();
    }
    return getInnerData().getValue(propName);
  }

  public void setValue(String propName, Object value) {
    throw new ReadonlyDataException();
  }

  protected void acceptChangeCore(IChangeDetail change) {
    throw new ReadonlyDataException();
  }

  public void clear() {
    throw new ReadonlyDataException();
  }

  @Override
  public boolean contains(String propName) {
    return getInnerData().contains(propName);
  }

  @Override
  public java.util.Iterator iterator() {
    if (getInnerData() == null) {
      throw new RuntimeException();
    }
    return getInnerData().iterator();
  }

  @Override
  protected AccessorBase createNewObject() {
    return new DynamicPropSetReadonlyAccessor();
  }
}
