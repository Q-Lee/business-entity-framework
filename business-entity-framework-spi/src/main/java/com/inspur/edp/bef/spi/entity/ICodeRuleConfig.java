/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.entity;

/** 
 BE编号规则设置类
 
*/
public interface ICodeRuleConfig
{
	/** 
	 保存时机设置编号字段，key字段名称，value编号规则信息类
	 
	 @return 
	*/
	java.util.HashMap<String, CodeRuleInfo> beforeSave();

	/** 
	 新建时机设置编号规则字段
	 
	 @return 
	*/
	java.util.HashMap<String, CodeRuleInfo> afterCreate();
}
