/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.action.assembler;

import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.cef.entity.entity.IEntityData;

/** 自定义动作组装器的默认实现，继承自IBEActionAssembler接口 */
public class BEActionAssembler implements IBEActionAssembler {
  private IBENodeEntityContext beContext;

  /**
   * 新实例初始化BEActionAssembler具有给定的实体上下文
   *
   * @param beContext 实体上下文
   */
  public BEActionAssembler(IBENodeEntityContext beContext) {
    this.beContext = beContext;
  }

  /** 自定义动作执行前记录日志时的虚扩展方法 */
  public void logBeforeExecute() {}

  /** 自定义动作执行前的虚扩展方法 */
  public void beforeExecute() {}

  /** 自定义动作执行后的虚扩展方法 */
  public void afterExecute() {}

  /** 自定义动作执行后记录日志时的虚扩展方法 */
  public void logAfterExecute() {}

  /**
   * 自定义动作执行抛异常时的虚扩展方法
   *
   * @param e 异常信息
   * @return
   */
  public boolean handleException(RuntimeException e) {
    return true;
  }

  /** 自定义动作执行失败时记录日志时的虚扩展方法 */
  public void logAfterExecuteFail() {}

  public boolean demandDataPermission(IEntityData data) {
    return true;
  }
}
