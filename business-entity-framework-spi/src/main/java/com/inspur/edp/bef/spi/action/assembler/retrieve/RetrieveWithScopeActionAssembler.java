/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.action.assembler.retrieve;

import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.spi.action.assembler.BEActionAssembler;

public class RetrieveWithScopeActionAssembler extends BEActionAssembler {
  public RetrieveWithScopeActionAssembler(IBEContext beContext, RetrieveParam para) {
    super(beContext);
    this.privateRetrieveParam = para;
  }

  private RetrieveParam privateRetrieveParam;

  protected final RetrieveParam getRetrieveParam() {
    return privateRetrieveParam;
  }
}
