/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.action.validation;

import com.inspur.edp.bef.api.action.validation.IDTValidation;
import com.inspur.edp.bef.api.action.validation.IValidationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.validation.AbstractValidationAction;

/** 业务实体上定义校验规则的抽象基类 */
public abstract class AbstractValidation extends AbstractValidationAction implements IDTValidation {
  /**
   * 新实例初始化AbstractValidation具有给定的校验规则上下文，以及实体数据变更集
   *
   * @param context 校验规则上下文
   * @param change 实体数据变更集
   */
  protected AbstractValidation(IValidationContext context, IChangeDetail change) {
    super(context, change);
  }

  /** 获取校验规则上下文，由构造函数传入 */
  @Override
  public IValidationContext getContext() {
    return (IValidationContext) super.getContext();
  }
}
