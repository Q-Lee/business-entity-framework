/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.action.assembler.entityAssemblerFactory;

import com.inspur.edp.bef.api.action.assembler.IAbstractActionAssemblerFactory;
import com.inspur.edp.bef.api.action.assembler.IBEActionAssembler;
import com.inspur.edp.bef.api.action.assembler.IDefaultValueProcessor;
import com.inspur.edp.bef.api.attr.AssemblerMethodAttribute;
import com.inspur.edp.bef.api.be.IBEContext;
import com.inspur.edp.bef.api.be.IBENodeEntityContext;
import com.inspur.edp.bef.api.parameter.retrieve.RetrieveParam;
import com.inspur.edp.bef.spi.action.assembler.delete.DeleteActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.delete.DeleteChildActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.modify.ModifyActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieve.AddLockActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieve.RetrieveActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieveDefault.RetrieveDefaultActionAssembler;
import com.inspur.edp.bef.spi.action.assembler.retrieveDefault.RetrieveDefaultActionChildAssembler;
import com.inspur.edp.bef.spi.action.assembler.save.SaveActionAssembler;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public interface IDefaultEntityActionAssFactory extends IAbstractActionAssemblerFactory {
  /**
   * 创建自定义动作的默认组装器
   *
   * @param beContext BE实体上下文
   * @return 自定义动作组装器基础接口
   */
  @AssemblerMethodAttribute("BaseActionAssembler")
  IBEActionAssembler getDefaultAssembler(IBENodeEntityContext beContext);
  /**
   * 实例化检索操作的默认组装器的虚方法
   *
   * @param beContext 实体上下文
   * @param para 检索参数
   * @return 检索操作的默认组装器
   */
  @AssemblerMethodAttribute("BefRetrieve")
  RetrieveActionAssembler getRetrieveActionAssembler(IBEContext beContext, RetrieveParam para);

  /**
   * 实例化加锁操作的默认组装器的虚方法
   *
   * @param beContext 实体上下文
   * @return 加锁操作的默认组装器
   */
  @AssemblerMethodAttribute("BefAddLock")
  AddLockActionAssembler getAddLockActionAssembler(IBEContext beContext);

  /**
   * 实例化修改操作的默认组装器的虚方法
   *
   * @param beContext 实体上下文
   * @param changeDetail 实体变更集
   * @return 修改操作的默认组装器
   */
  @AssemblerMethodAttribute("BefModify")
  ModifyActionAssembler getModifyActionAssembler(IBEContext beContext, IChangeDetail changeDetail);

  /**
   * 实例化保存操作的默认组装器的虚方法
   *
   * @param beContext 实体上下文
   * @return 保存操作的默认组装器
   */
  @AssemblerMethodAttribute("BefSave")
  SaveActionAssembler getSaveActionAssembler(IBEContext beContext);

  /**
   * 实例化新增操作的默认组装器的虚方法
   *
   * @param beContext 实体上下文
   * @return 新增操作的默认组装器
   */
  @AssemblerMethodAttribute("BefRetrieveDefault")
  RetrieveDefaultActionAssembler getRetrieveDefaultActionAssembler(IBEContext beContext);

  /**
   * 实例化新增从(从)表数据操作的默认组装器的虚方法
   *
   * @param beContext 实体上下文
   * @param nodeCodes 要新增数据的从(从)表的实体编号
   * @param hierachyIdList 新增从(从)表数据的所属实体数据的唯一标识
   * @return 新增从(从)表数据操作的默认组装器
   */
  @AssemblerMethodAttribute("BefRetrieveDefaultChild")
  RetrieveDefaultActionChildAssembler getRetrieveDefaultChildActionAssembler(
      IBEContext beContext,
      java.util.List<String> nodeCodes,
      java.util.List<String> hierachyIdList);

  /**
   * 实例化删除操作的默认组装器的虚方法
   *
   * @param beContext 实体上下文
   * @return 删除操作的默认组装器
   */
  @AssemblerMethodAttribute("BefDelete")
  DeleteActionAssembler getDeleteActionAssembler(IBEContext beContext);

  /**
   * 实例化删除子表数据操作的默认组装器的虚方法
   *
   * @param beContext 实体上下文
   * @param nodeCodes 要删除数据的从(从)表的实体编号
   * @param hierachyIdList 要删除从(从)表数据的所属实体数据的唯一标识
   * @param ids 要删除的从(从)表数据的唯一标识集合
   * @return 删除子表数据操作的默认组装器
   */
  @AssemblerMethodAttribute("BefDeleteChild")
  DeleteChildActionAssembler getDeleteChildActionAssembler(
      IBEContext beContext,
      java.util.List<String> nodeCodes,
      java.util.List<String> hierachyIdList,
      java.util.List<String> ids);

  /**
   * 实例化默认值处理器的虚方法
   *
   * @return 默认值处理器
   */
  IDefaultValueProcessor getDefaultValueProcessor(IBEContext beContext);
}
