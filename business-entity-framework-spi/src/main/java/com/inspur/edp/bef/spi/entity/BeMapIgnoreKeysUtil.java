/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.entity;

import java.util.Map;
import java.util.Set;

public class BeMapIgnoreKeysUtil {

    public static boolean containsIgnoreKey(Set<String> keys, String propertyName) {
        if (keys == null || propertyName == null) {
            return false;
        }
        for (String key : keys) {
            if (propertyName.equalsIgnoreCase(key)) {
                return true;
            }
        }
        return false;
    }

    public static <T> T getValueByIgnoreKey(Map<String, T> keys, String propertyName) {
        if (keys == null || propertyName == null) {
            return null;
        }
        for (Map.Entry<String, T> entry : keys.entrySet()) {
            if (propertyName.equalsIgnoreCase(entry.getKey())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public static String getRelKey(Set<String> keys, String propertyName) {
        if (keys == null || propertyName == null) {
            return "";
        }
        for (String key : keys) {
            if (propertyName.equalsIgnoreCase(key)) {
                return key;
            }
        }
        return "";
    }
}
