/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.event.queryactionevent;

import com.inspur.edp.bef.api.be.IBEManager;
import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;

import java.util.ArrayList;
import java.util.List;

public class BefQueryEventBroker {
    private static List<IBefQueryEventListener> eventLs = new ArrayList<IBefQueryEventListener>() {{
        //add(getBefQueryEventListener("com.inspur.edp.bef.debugtool.listener.impl.query.BefQueryEventListener"));
    }};

    private static IBefQueryEventListener getBefQueryEventListener(String className) {
        try {
            Class cls = Class.forName(className);
            IBefQueryEventListener listener = (IBefQueryEventListener) cls.newInstance();
            return listener;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new RuntimeException("befquerylistener实例初始化失败", e);
        }
    }

    public static final void fireBeforeQuery(IBEManager manager, EntityFilter filter) {
        for (IBefQueryEventListener el : eventLs) {
            el.beforeQuery(manager, filter);
        }
    }

    public static final void fireBeforeTotalCXQDeterminate(EntityFilter filter, EntityFilter customfilter, EntityFilter totalFilter) {
        for (IBefQueryEventListener el : eventLs) {
            el.beforeTotalCXQDeterminate(filter, customfilter, totalFilter);
        }
    }

    public static final void fireBeforeCXQDeterminate(EntityFilter beforefilter) {
        for (IBefQueryEventListener el : eventLs) {
            el.beforeCXQDeterminate(beforefilter);
        }
    }

    public static final void fireAfterCXQDeterminate(EntityFilter afterfilter) {
        for (IBefQueryEventListener el : eventLs) {
            el.afterCXQDeterminate(afterfilter);
        }
    }

    public static final void fireAfterTotalCXQDeterminate(EntityFilter afterfilter, int TotalCount) {
        for (IBefQueryEventListener el : eventLs) {
            el.afterTotalCXQDeterminate(afterfilter, TotalCount);
        }
    }


    public static final void firebeforeCoreQuery(EntityFilter finalfilter) {
        for (IBefQueryEventListener el : eventLs) {
            el.beforeCoreQuery(finalfilter);
        }
    }

    public static final void fireProcessGetAuthority(ArrayList<AuthorityInfo> auls) {
        for (IBefQueryEventListener el : eventLs) {
            el.processGetAuthority(auls);
        }
    }

    public static final void fireafterCoreQuery(IBEManager bemanager, List<IEntityData> resultvar) {
        for (IBefQueryEventListener el : eventLs) {
            el.afterCoreQuery(bemanager, resultvar);
        }
    }

    public static final void fireBeforeTotalCXHDeterminate(IBEManager bemanager,List<IEntityData> beforedatas) {
        for (IBefQueryEventListener el : eventLs) {
            el.beforeTotalCXHDeterminate(bemanager, beforedatas);
        }
    }

    public static final void fireBeforeDataCXHDeterminate(IBEManager bemanager, ICefDeterminationContext context){
        for (IBefQueryEventListener el : eventLs) {
            el.beforeDataCXHDeterminate(bemanager,context);
        }

    }

    public static final void fireAfterDataCXHDeterminate(IBEManager bemanager, ICefDeterminationContext context, int TotalCount){
        for (IBefQueryEventListener el : eventLs) {
            el.afterDataCXHDeterminate(bemanager,context,TotalCount);
        }

    }

    public static final void firebeforeCXHDeterminate(IBEManager bemanager, ICefDeterminationContext context) {
        for (IBefQueryEventListener el : eventLs) {
            el.beforeCXHDeterminate(bemanager, context);
        }
    }

    public static final void fireafterCXHDeterminate(IBEManager bemanager, ICefDeterminationContext context) {
        for (IBefQueryEventListener el : eventLs) {
            el.afterCXHDeterminate(bemanager, context);
        }
    }

    public static final void fireafterTotalCXHDeterminate(IBEManager bemanager, List<IEntityData> resultvar, int TotalCount) {
        for (IBefQueryEventListener el : eventLs) {
            el.afterTotalCXHDeterminate(bemanager, resultvar, TotalCount);
        }
    }

    public static final void fireAfterQuery() {
        for (IBefQueryEventListener el : eventLs) {
            el.afterQuery();
        }
    }

    public static final void fireQueryUnnormalStop(Exception e) {
        for (IBefQueryEventListener el : eventLs) {
            el.queryUnnormalStop(e);
        }
    }

    public static final void fireProcessRecordClassPath(AbstractDeterminationAction determination) {
        for (IBefQueryEventListener el : eventLs) {
            el.processRecordClassPath(determination);
        }
    }

}
