/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.entity;

import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import com.inspur.edp.cef.spi.jsonser.abstractcefchange.AbstractCefDataDeSerializer;
import com.inspur.edp.cef.spi.jsonser.entity.AbstractEntitySerializerItem;
import java.util.ArrayList;

/**
 * 变更集序列化
 */
public class BefChangeDeserConvertor extends AbstractBizEntityChangeJsonDeSerializer {
    private final String code;
    private final boolean isRoot;
    private final String type;
    private final CefEntityResInfoImpl info;

    public BefChangeDeserConvertor(String objectCode, boolean isRoot, String beType, CefEntityResInfoImpl resourceInfo){
        super(objectCode, isRoot, beType, getTypes(resourceInfo));
        this.code = objectCode;
        this.isRoot = isRoot;
        this.type = beType;
        this.info = resourceInfo;
    }
    private static java.util.ArrayList<AbstractEntitySerializerItem> getTypes(CefEntityResInfoImpl resourceInfo){
        java.util.ArrayList<AbstractEntitySerializerItem> list=new ArrayList<>();
        list.add(new com.inspur.edp.cef.spi.jsonser.builtinimpls.CefEntityDataSerializerItem(resourceInfo));
        return list;
    }

    @Override
    protected
    com.inspur.edp.cef.spi.jsonser.entity.AbstractEntityChangeJsonDeSerializer getChildEntityConvertor(
        java.lang.String nodeCode) {
        if (info.getChildEntityResInfos() != null && BeMapIgnoreKeysUtil.containsIgnoreKey(info.getChildEntityResInfos().keySet(), nodeCode)) {
            return new BefChangeDeserConvertor(BeMapIgnoreKeysUtil.getRelKey(info.getChildEntityResInfos().keySet(), nodeCode), false, type, (CefEntityResInfoImpl) BeMapIgnoreKeysUtil.getValueByIgnoreKey(info.getChildEntityResInfos(), nodeCode));
        }
        // todo 详细注释
        throw new RuntimeException("无法找到"+ nodeCode +"节点");
    }

    @Override
    protected boolean isChildObjectCode(String propertyName) {
        // todo 判断是否存在true
        String name = propertyName;
        if ("s".equalsIgnoreCase(propertyName.substring(propertyName.length() - 1))) {
            name = propertyName.substring(0, propertyName.length() - 1);
        }
        return info.getChildEntityResInfos() != null && BeMapIgnoreKeysUtil.containsIgnoreKey(info.getChildEntityResInfos().keySet(), name);
    }

    @Override
    protected AbstractCefDataDeSerializer getCefDataDeSerializer() {
        return new BefEntityDeserConvertor(code, isRoot, type, info);
    }
}
