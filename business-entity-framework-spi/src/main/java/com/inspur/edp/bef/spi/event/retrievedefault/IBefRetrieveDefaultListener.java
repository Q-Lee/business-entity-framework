/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.bef.spi.event.retrievedefault;

import com.inspur.edp.bef.api.be.IBEManagerContext;
import com.inspur.edp.bef.api.be.IBusinessEntity;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.spi.determination.AbstractDeterminationAction;
import java.util.Map;

public interface IBefRetrieveDefaultListener {

    void beforeRetrieveDefaultManagerAction(IBEManagerContext context,boolean useId,String dataId,
        Map<String,Object> defaultValues);

    void afterRetrieveDefaultManagerAction( IBEManagerContext context,IEntityData data);

    void beforeDealDefaultValue(IBusinessEntity be,Map<String, Object> defaultValues,IEntityData data);

    void afterDealDefaultValue(IBusinessEntity be,Map<String, Object> defaultValues,IEntityData data);

    void beforeRetrieveDefaultDetermination( AbstractDeterminationAction action);

    void afterRetrieveDefaultDetermination( IEntityData data);

    void exceptionStop(Exception e);
}
