/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.scope;

import com.inspur.edp.cef.api.scope.ICefScopeNodeParameter;


public class CefScopeNode
{

		///#region Constructor
	public CefScopeNode()
	{
	}
	/**
	 BEFScopeNodeID
	*/
	private String privateID;
	public final String getID()
	{
		return privateID;
	}
	public final void setID(String value)
	{
		privateID = value;
	}

	/** 
	 参数列表，按照存入顺序执行,aaabbbcccaaaa，先执行aaa,再执行bbb
	 
	*/
	private java.util.ArrayList<ICefScopeNodeParameter> listPara;

	/**
	 获取所有Scope参数
	 
	 @return 
	*/
	public final java.util.ArrayList<ICefScopeNodeParameter> getAllParameters()
	{
		return listPara;
	}

	/** 
	 新增Para
	 
	 @param paraType
	 @param para
	*/
	public final void setParameter(ICefScopeNodeParameter para)
	{
		if (listPara == null)
		{
			listPara = new java.util.ArrayList<ICefScopeNodeParameter>();
		}
		listPara.add(para);
	}

	public final void setPreScopeNode()
	{
		throw new UnsupportedOperationException();
	}
}
