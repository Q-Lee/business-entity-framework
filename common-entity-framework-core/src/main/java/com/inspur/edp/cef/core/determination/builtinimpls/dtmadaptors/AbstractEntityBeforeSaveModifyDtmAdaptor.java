/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.determination.builtinimpls.dtmadaptors;

import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.ChildElementsTuple;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import java.util.List;

public abstract class AbstractEntityBeforeSaveModifyDtmAdaptor extends AbstractBeforeSaveDtmAdaptor {

  private final List<ChildElementsTuple> childElementsTuples;

  public AbstractEntityBeforeSaveModifyDtmAdaptor(String name,
      List<ChangeType> changeTypes,
      List<String> elements,List<ChildElementsTuple> childElementsTuples) {
    super(name, changeTypes, elements);
    this.childElementsTuples = childElementsTuples;
  }

  @Override
  public boolean canExecute(IChangeDetail change) {
    return super.canExecute(change);
  }
}
