/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.validation.builtinimpls;

import com.inspur.edp.cef.api.dataType.base.ICefDataTypeContext;
import com.inspur.edp.cef.api.dataType.entity.ICefEntityContext;
import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.validation.IEntityRTValidationAssembler;
import com.inspur.edp.cef.spi.validation.IValidation;
import java.util.ArrayList;
import java.util.List;

public abstract class CefAbstractValAssembler implements IEntityRTValidationAssembler {

  private List<IValidation> belongingVals;
  @Override
  public List<IValidation> getBelongingValidations() {
    if(belongingVals==null)
      belongingVals=new ArrayList<>();
    return belongingVals;
  }

public void addBelongingVals(IValidation validation)
{
  getBelongingValidations().add(validation);
}

  public final  void insertBelongingVals(IValidation validation,int index)
  {getBelongingValidations().add(index,validation);}

private List<IValidation> validations;
  @Override
  public List<IValidation> getValidations() {
    if(validations==null)
      validations=new ArrayList<>();
    return validations;
  }

  public final void addValidations(IValidation validation)
  {
    getValidations().add(validation);
  }

  private List<IValidation> childAssemblers;
  @Override
  public List<IValidation> getChildAssemblers() {
    if(childAssemblers==null)
      childAssemblers=new ArrayList<>();
    return childAssemblers;
  }
}
