/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.determination.builtinimpls.dtmadaptors;

import com.inspur.edp.cef.api.determination.ICefDeterminationContext;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.ChildElementsTuple;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.determination.IDetermination;
import com.inspur.edp.cef.spi.determination.IDeterminationExecutor;
import java.util.List;

public abstract class AbstractAfterModifyDtmAdaptor implements IDetermination {

  private final String name;
  private final List<ChangeType> changeTypes;
  private final List<String> elements;

  public AbstractAfterModifyDtmAdaptor(String name, List<ChangeType> changeTypes,List<String> elements)
  {

    this.name = name;
    this.changeTypes = changeTypes;
    this.elements = elements;
  }
  @Override
  public String getName() {
    return name;
  }

  @Override
  public boolean canExecute(IChangeDetail change) {
    return false;
//    if(changeTypes.contains(change.getChangeType())==false)
//      return false;
//    if(change.getChangeType()!=ChangeType.Modify)
//      return true;
//    if(elements!=null||elements.size()>0)
//      return elements
  }

}
