/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.data;

import com.inspur.edp.cef.entity.accessor.base.ReadonlyDataException;
import com.inspur.edp.cef.entity.accessor.entity.IChildAccessor;
import com.inspur.edp.cef.entity.accessor.entity.IEntityAccessor;
import com.inspur.edp.cef.entity.entity.IChildEntityData;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.entity.entity.IEntityDataCollection;
import com.inspur.edp.cef.spi.entity.resourceInfo.builinImpls.CefEntityResInfoImpl;
import java.util.function.Function;
import java.util.function.Supplier;

public class AccessorReadonlyCollection extends
    AccessorCollection {
  public AccessorReadonlyCollection(IEntityAccessor belongEntity, CefEntityResInfoImpl resInfo,
      Supplier<IChildEntityData> dataCreator,
      Function<IChildEntityData, IChildAccessor> accCreator) {
    super(belongEntity, resInfo, dataCreator, accCreator);
  }

  @Override
  protected com.inspur.edp.cef.entity.accessor.entity.AccessorCollection createNewObject() {
    return new AccessorReadonlyCollection(null, resInfo, dataCreator, accCreator);
  }

  @Override
  protected final void beforeAdd(IEntityData item)
  {
    throw new ReadonlyDataException();
  }

  @Override
  protected final void beforeRemove(IEntityData item)
  {
    throw new ReadonlyDataException();
  }

  @Override
  protected final void beforeClear()
  {
    throw new ReadonlyDataException();
  }
}
