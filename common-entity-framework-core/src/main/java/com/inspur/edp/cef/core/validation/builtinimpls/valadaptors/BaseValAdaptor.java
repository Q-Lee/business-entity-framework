/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.validation.builtinimpls.valadaptors;

import com.inspur.edp.cef.api.validation.ICefValidationContext;
import com.inspur.edp.cef.core.action.CompReflector;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.spi.validation.AbstractValidationAction;
import com.inspur.edp.cef.spi.validation.IValidation;
import java.util.Objects;

public abstract class BaseValAdaptor implements IValidation {

  private final Class type;
  private final CompReflector reflector;

  public BaseValAdaptor(Class type, CompReflector reflector) {
    this.type = type;
    this.reflector = reflector;
  }

  @Override
  public void execute(ICefValidationContext ctx, IChangeDetail change) {
    Objects.requireNonNull(reflector, "reflector");
    AbstractValidationAction comp = (AbstractValidationAction) reflector
        .createInstance(ctx, change);
    comp.Do();
  }
}
