/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.scope;

import com.inspur.edp.cef.core.common.LongGenerator;

public abstract class CefScope {

  protected String id;
  private CefScopeManager scopeManager;

  public CefScope() {
    id = LongGenerator.getString();
  }

  private CefScopeManager getScopeManager() {
    if (scopeManager == null) {
      scopeManager = createScopeManagerInstance();
    }
    return scopeManager;
  }

  public final void beginScope() {
    getScopeManager().beginScope(id);
  }

  public final void setAbort() {
    getScopeManager().setAbort(id);
  }

  public final void setComplete() {
    getScopeManager().setComplete(id);
  }

  protected abstract CefScopeManager createScopeManagerInstance();
}
