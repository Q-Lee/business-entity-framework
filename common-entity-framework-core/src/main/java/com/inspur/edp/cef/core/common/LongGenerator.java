/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.common;

import java.time.LocalTime;
import java.util.concurrent.atomic.AtomicLong;

public final class LongGenerator {

  private static AtomicLong value = new AtomicLong(LocalTime.now().hashCode());

  private LongGenerator() {
  }

  public static final long get() {
    return value.incrementAndGet();
  }

  public static final String getString() {
    return Long.toString(get());
  }
}
