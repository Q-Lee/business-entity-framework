/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.data;

import com.inspur.edp.cef.core.data.extendhandler.CefDataExtHandler;
import com.inspur.edp.cef.core.data.extendhandler.ValueObjDataExtHandler;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.cef.entity.entity.IValuesContainer;

public abstract class ValueObjDataBase extends CefDataBase implements IValueObjData, IValuesContainer {

  //region ext
  @Override
  protected CefDataExtHandler createExtHandler() {
    return new ValueObjDataExtHandler();
  }

  @Override
  protected ValueObjDataExtHandler getExtHandler() {
    return (ValueObjDataExtHandler) super.getExtHandler();
  }

  @Override
  public Object createValue(String propName) {
    return innerCreateValue(propName);
  }

  protected Object innerCreateValue(java.lang.String propName){
    return null;
  }

  //endregion
}
