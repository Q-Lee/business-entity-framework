/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.core.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.core.data.extendhandler.ValueObjAccExtHandler;
import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.changeset.ChangeType;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.ValueObjModifyChangeDetail;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.entity.entity.IValueObjData;
import com.inspur.edp.cef.spi.entity.resourceInfo.DataTypeResInfo;
import com.inspur.edp.cef.spi.extend.datatype.ICefDataExtend;
import com.inspur.edp.cef.spi.extend.datatype.IDataExtendContainer;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public abstract class ValueObjAccessor extends
    com.inspur.edp.cef.entity.accessor.dataType.ValueObjAccessor implements IValueObjData,
    IDataExtendContainer {

  protected ValueObjAccessor() {
  }

  protected ValueObjAccessor(IValueObjData inner) {
    super(inner);
    //2021年7月7日值对象暂无扩展场景, 且据某性能分析认为较影响性能, 按要求先注释
    //initExtByInner(inner);
  }

  @Override
  public ICefData copy() {
    ValueObjAccessor rez = (ValueObjAccessor) super.copy();
    rez.setPropName(getPropName());
    //此处应参考entityAccessor在handler上增加copy方法直接转调
    rez.extHandler = null;
    List<ICefDataExtend> currentExts = getExtends();
    if(currentExts != null) {
      rez.addExtend(currentExts.toArray(new ICefDataExtend[currentExts.size()]));
    }
    return rez;
  }

  //region
  private ValueObjAccExtHandler extHandler;

  private ValueObjAccExtHandler getExtHandler() {
    if (extHandler == null) {
      extHandler = new ValueObjAccExtHandler(this, this.getIsReadonly());
    }
    return extHandler;
  }

  @Override
  public void addExtend(ICefDataExtend... ext) {
    Objects.requireNonNull(ext, "ext");

    Arrays.stream(ext).forEach(item -> getExtHandler().addExtend(item));
  }

  @Override
  @JsonIgnore
  public List<ICefDataExtend> getExtends() {
    if (extHandler == null) {
      return null;
    }
    return extHandler.getExtendList();
  }

  private void initExtByInner(ICefData inner) {
    if (inner != null && inner instanceof IDataExtendContainer) {
      List<ICefDataExtend> exts = ((IDataExtendContainer) inner).getExtends();
      if (exts != null) {
        exts.stream().forEach(ext -> addExtend(ext));
      }
    } else {
      getExtHandler().clear();
    }
  }
  //endregion

  //region get/set Value
  @Override
  public final Object getValue(String s) {
    RefObject<Object> result = new RefObject<>(null);
    if (extHandler != null && extHandler.getValue(s, result)) {
      return result.argvalue;
    }
    return innerGetValue(s);
  }

  protected abstract Object innerGetValue(String propName);

  @Override
  public final void setValue(String name, Object value) {
    if (extHandler != null && extHandler.setValue(name, value)) {
      return;
    }
    innerSetValue(name, value);
  }

  protected abstract void innerSetValue(String name, Object value);

  @Override
  public final ICefData copySelf() {
    return innerCopySelf();
  }

  protected ICefData innerCopySelf() {
    return getInnerData().copySelf();
  }

  @Override
  protected void acceptChangeCore(IChangeDetail change) {
    checkReadonly();
    if(extHandler != null){
      extHandler.acceptChange(change);
    }

    if(getResInfo() != null) {
      if (change.getChangeType() == ChangeType.Modify) {
        ValueObjModifyChangeDetail modifyChangeDetail = (ValueObjModifyChangeDetail) change;
        for (Map.Entry<String, Object> entry : modifyChangeDetail.getPropertyChanges().entrySet()) {
          if (entry.getValue() instanceof IChangeDetail)
            ((IAccessor) getValue(entry.getKey())).acceptChange((IChangeDetail) entry.getValue());
          else
            setValue(entry.getKey(), entry.getValue());
        }
        return;
      } else {
        throw new IllegalArgumentException("change");
      }
    }
  }

  protected DataTypeResInfo getResInfo(){
    return null;
  }

  @Override
  protected void onInnerDataChange() {
    super.onInnerDataChange();
    getExtHandler().onInnerDataChanged();
  }
  //endregion
}
