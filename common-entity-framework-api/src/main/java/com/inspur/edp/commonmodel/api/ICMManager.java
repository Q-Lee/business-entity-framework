/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.commonmodel.api;

import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;
import java.util.List;

public interface ICMManager {
  // ORIGINAL LINE: IEntityData CreateData(string dataID = "");
  IEntityData createData(String dataID);
  // ORIGINAL LINE: IEntityData CreateChildData(string childCode, string dataID = "");
  IEntityData createChildData(String childCode, String dataID);

  // ORIGINAL LINE: IEntityData CreateUnlistenableData(string dataID = "");
  IEntityData createUnlistenableData(String dataID);
  // ORIGINAL LINE: IEntityData CreateUnlistenableChildData(string childCode, string dataID = "");
  IEntityData createUnlistenableChildData(String childCode, String dataID);

  String serializeData(IEntityData data);

  String serializeDataForExpression(IEntityData data);

  IEntityData deserializeData(String serializedData);

  //子表的序列化及反序列化
  String serializeData(List<String> nodeCodes,IEntityData data);

  IEntityData deserializeData(List<String> nodeCodes,String serializedData);

  String serializeChanges(List<IChangeDetail> changes);

  List<IChangeDetail> deserializeChanges(String strChanges);
  String serializeChange(IChangeDetail change);
  IChangeDetail deserializeChange(String changeStr);

}
