/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.message;

public class MessageLocation
{
	private String privateNodeCode;
	public final String getNodeCode()
	{
		return privateNodeCode;
	}
	public final void setNodeCode(String value)
	{
		privateNodeCode = value;
	}

	private java.util.List<String> dataIds;
	/** 
	 消息对应的数据行
	 
	*/
	public final java.util.List<String> getDataIds()
	{
		return (dataIds != null) ? dataIds : (dataIds = new java.util.ArrayList<String>());
	}
	public final void setDataIds(java.util.List<String> value)
	{
		dataIds = value;
	}

	private java.util.List<String> columnNames;
	/** 
	 消息对应的数据列
	 
	*/
	public final java.util.List<String> getColumnNames()
	{
		return (columnNames != null) ? columnNames : (columnNames = new java.util.ArrayList<String>());
	}
	public final void setColumnNames(java.util.List<String> value)
	{
		columnNames = value;
	}

	//消息产生时行上数据的值
	//public IList<Tuple<string, string>> Elements { get; set; }
}
