/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.action;

import java.util.Map;

public class ActionExecuteContext {
    private String actionId;
    private String actionCode;
    private Map<String, Object> parameters;

    public ActionExecuteContext(String actionId, String actionCode, Map<String,Object> parameters)
    {
        this();
        this.actionId = actionId;
        this.actionCode = actionCode;
        this.parameters = parameters;
    }

    private ActionExecuteContext()
    {}

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    private ActionExecuteContext preContext;

    public ActionExecuteContext getPreContext() {
        return preContext;
    }

    public void setPreContext(ActionExecuteContext preContext) {
        this.preContext = preContext;
    }
}
