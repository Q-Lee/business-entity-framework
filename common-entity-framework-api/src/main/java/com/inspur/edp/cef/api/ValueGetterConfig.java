/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api;
import lombok.Builder;
import lombok.Data;
/**
 * @author Jakeem
 * @Data 2019/12/27 - 9:51
 */

@Builder(toBuilder=true)
public
class ValueGetterConfig {
    //单值udt
    private boolean singleValueUdt;
    //关联
    private boolean associationValue;

    public
    ValueGetterConfig() {
        this.singleValueUdt =  true;
        this.associationValue = true;
    }

    ValueGetterConfig(boolean singleValueUdt, boolean associationValue) {
        this.singleValueUdt = singleValueUdt;
        this.associationValue = associationValue;

    }

    public
    boolean getSingleValueUdt() {
        return this.singleValueUdt;
    }

    public
    boolean getAssociationValue() {
        return this.associationValue;
    }


    public
    void setSingleValueUdt(boolean value) {
        this.singleValueUdt = value;
    }

    public
    void setAssociationValue(boolean associationValue) {
        this.associationValue = associationValue;
    }



}
