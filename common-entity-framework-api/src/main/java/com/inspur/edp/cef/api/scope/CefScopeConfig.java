/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.scope;

public class CefScopeConfig
{
	private java.util.ArrayList<ScopeExtensionConfig> privateScopeExtensions;
	public final java.util.ArrayList<ScopeExtensionConfig> getScopeExtensions()
	{
		return privateScopeExtensions;
	}
	public final void setScopeExtensions(java.util.ArrayList<ScopeExtensionConfig> value)
	{
		privateScopeExtensions = value;
	}
}
