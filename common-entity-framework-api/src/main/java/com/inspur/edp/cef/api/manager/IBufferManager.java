/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.manager;

import com.inspur.edp.cef.entity.accessor.base.IAccessor;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.entity.IEntityData;

public interface IBufferManager {

  IAccessor getBuffer(String dataId, int level);

  IAccessor getBuffer(String dataId);

  IAccessor initBufferByBaseLevel(String id, int level, boolean isReadonly);

  IAccessor initBufferByLevel(IEntityData data, int level, boolean isReadonly);

  IAccessor initBuffer_ZeroLevel(IEntityData data, boolean isReadonly);

  IAccessor accept(String dataId, int level, IChangeDetail change);

  IAccessor reject(String dataId, int level, IChangeDetail change);

  void rejectBufferZeroLevel(String id);

  void remove(String dataId);
}
