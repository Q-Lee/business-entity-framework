/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.scope;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ScopeExtensionConfig {

  public ScopeExtensionConfig(String id, String className){
    setID(id);
    setClassName(className);
  }

  // 与scopeNodeParameter.ParameterType匹配
  private String privateID;

  public final String getID() {
    return privateID;
  }

  public final void setID(String value) {
    privateID = value;
  }

  // 程序集
  private String privateAssembly;

  @Deprecated
  public final String getAssembly() {
    return privateAssembly;
  }

  @Deprecated
  public final void setAssembly(String value) {
    privateAssembly = value;
  }

  // 类名
  private String privateClassName;

  public final String getClassName() {
    return privateClassName;
  }

  public final void setClassName(String value) {
    privateClassName = value;
  }

  private Object lock = new Object();
  private Constructor ctor;
  public Object getExtInstance(){
    if(ctor == null){
      synchronized (lock){
        if(ctor == null){
          try {
            ctor = Class.forName(getClassName()).getConstructors()[0];
          } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
          }
        }
      }
    }
    try {
      return ctor.newInstance();
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      throw new RuntimeException(e);
    }
  }
}
