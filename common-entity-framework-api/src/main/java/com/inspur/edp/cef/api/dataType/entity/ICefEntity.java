/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.dataType.entity;


import com.inspur.edp.cef.api.dataType.base.ICefDataType;

public interface ICefEntity extends ICefDataType
{
	String getID();

//region 联动计算
	void retrieveDefaultDeterminate();

	void afterModifyDeterminate();

	void beforeSaveDeterminate();
		//endregion


		//region 校验规则
	void afterModifyValidate();

	void beforeSaveValidate();

		//endregion

	ICefEntity getChildEntity(String code, String id);
}
