/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.validation;

import com.inspur.edp.cef.entity.entity.ICefData;

public interface ICefValidationContext {
  ICefData getData();

  // region i18n
  /**
   * 翻译后的实体名称
   *
   * @return
   */
  String getEntityI18nName();

    /**
     * 翻译后的属性名称
     *
     * @param labelID
     * @return
     */
    String getPropertyI18nName(String labelID);

    /**
   * 翻译后的关联带出字段名称
   *
   * @param labelID
   * @param refLabelID
   * @return
   */
  String getRefPropertyI18nName(String labelID, String refLabelID);
  /**
   * 翻译后的枚举显示值
   *
   * @param labelID
   * @param enumKey
   * @return
   */
  String getEnumValueI18nDisplayName(String labelID, String enumKey);

  // endregion
}
