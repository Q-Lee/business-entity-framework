/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.association;

import java.util.HashMap;
import java.util.Map;

public abstract class CefBaseAssociation {
    private AssociationRefInfo associationRefInfo;
    private Map<String,String> refProperties=new HashMap<>();
    private Map<String,String> assoMaps =new HashMap<>();

    public final AssociationRefInfo getAssociationRefInfo() {
        return associationRefInfo;
    }

    public final void setAssociationRefInfo(AssociationRefInfo associationRefInfo) {
        this.associationRefInfo = associationRefInfo;
    }


    public final Map<String, String> getRefProperties() {
        return refProperties;
    }

    public final Map<String, String> getAssoMaps() {
        return assoMaps;
    }
}
