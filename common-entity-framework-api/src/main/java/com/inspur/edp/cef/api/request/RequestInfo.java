/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.request;


import com.inspur.edp.cef.entity.changeset.IChangeDetail;

public class RequestInfo {
  private java.util.ArrayList<IChangeDetail> privateDataChange =
      new java.util.ArrayList<IChangeDetail>();

  public final java.util.ArrayList<IChangeDetail> getDataChange() {
    return privateDataChange;
  }

  private IChangeDetail privateVariableChange;

  public final IChangeDetail getVariableChange() {
    return privateVariableChange;
  }

  public final void setVariableChange(IChangeDetail value) {
    privateVariableChange = value;
  }
}
