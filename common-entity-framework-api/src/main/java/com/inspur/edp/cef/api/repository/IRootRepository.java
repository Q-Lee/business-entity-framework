/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.repository;

import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.api.authority.AuthorityInfo;
import com.inspur.edp.cef.api.repository.adaptor.IDataAdapterExtendInfo;
import com.inspur.edp.cef.entity.UQConstraintMediate;
import com.inspur.edp.cef.entity.changeset.IChangeDetail;
import com.inspur.edp.cef.entity.changeset.Tuple;
import com.inspur.edp.cef.entity.condition.EntityFilter;
import com.inspur.edp.cef.entity.condition.NodeSortInfo;
import com.inspur.edp.cef.entity.condition.RetrieveFilter;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.api.repository.dac.IDataTypeDac;

import com.inspur.edp.cef.entity.repository.DataSaveParameter;
import com.inspur.edp.cef.entity.repository.DataSaveResult;
import io.iec.edp.caf.databaseobject.api.entity.TempTableContext;
import java.util.*;
import java.sql.SQLException;

public interface IRootRepository
{
	void initParams(java.util.Map<String, Object> pars);

	void initRepoVariables(java.util.HashMap<String, String> vars);

	/**
	 Query方法

	 @param filter 过滤条件
	 @param authorities 权限信息
	 @return
	 */
	java.util.List<IEntityData> query(EntityFilter filter,
									  java.util.ArrayList<AuthorityInfo> authorities);

	java.util.List<IEntityData> query(String entityCode, EntityFilter filter,
									  java.util.ArrayList<AuthorityInfo> authorities);

	IEntityData retrieve(String id) throws SQLException;
	/**
	 支持从表，从从表排序

	 @param id 传入主表Id
	 @param sortCondition 传入从表
	 @return
	 */
	IEntityData retrieve(String id, java.util.ArrayList<NodeSortInfo> sortCondition) throws SQLException;
	IEntityData retrieve(String id, ArrayList<NodeSortInfo> nodeSortInfos, RetrieveFilter retrieveFilter);
	/**
	 retrieve

	 @param dataIds 数据Id
	 @return
	 */
	java.util.List<IEntityData> retrieve(java.util.List<String> dataIds) throws SQLException;
	List<IEntityData> retrieve(List<String> dataIds, RetrieveFilter retrieveFilter);

	List<DataSaveResult> save(List<Tuple<IChangeDetail, DataSaveParameter>> changes) throws SQLException;

	void save(IChangeDetail... changes) throws SQLException;

	String getTableNameByColumns(String nodeCode, java.util.HashMap<String, String> columns, String keyColumnName, RefObject<String> keyDbColumnName, java.util.ArrayList<String> tableAlias);

	String getTableNameByRefColumns(String nodeCode, java.util.HashMap<String, RefColumnInfo> columns, String keyColumnName, RefObject<String> keyDbColumnName, java.util.ArrayList<String> tableAlias);

	IDataTypeDac getMainDac();

	boolean isRef(String nodeCode, String dataId, String propName);

	boolean isEffective(String nodeCode, String dataId);
	boolean isDatasEffective(String nodeCode, java.util.ArrayList<String> dataIds, RefObject<java.util.ArrayList<String>> noneEffectiveDatas);
	boolean isDatasEffective(String nodeCode, java.util.ArrayList<String> dataIds, RefObject<java.util.ArrayList<String>> noneEffectiveDatas, String propName);

	void generateTable(String nodeCode);

	void dropTable(String nodeCode);

	void checkUniqueness(HashMap<String, ArrayList<UQConstraintMediate>> constraintInfos);

	void checkUniqueness(HashMap<String, ArrayList<UQConstraintMediate>> constraintInfos, boolean throwEx);

	Date getVersionControlValue(String dataId);
	HashMap<String, Date> getVersionControlValues(List<String> dataIds);
	void addExtendInfo(IDataAdapterExtendInfo extendInfo);

	TempTableContext createTempTable(String nodeCode);

	void dropTempTable(String nodeCode,TempTableContext tempTableContext);

	List<String> getDistinctFJM(String fjnPropertyName,EntityFilter filter,ArrayList<AuthorityInfo> authorityInfos,Integer parentLayer);
}
