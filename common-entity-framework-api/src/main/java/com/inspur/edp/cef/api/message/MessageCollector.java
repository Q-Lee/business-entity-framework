/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.api.message;

import java.util.Collections;

public class MessageCollector implements IMessageCollector {

  private java.util.ArrayList<IBizMessage> messages = new java.util.ArrayList<IBizMessage>();

  public static IBizMessage createMessage(String msg, String[] msgPars, MessageLevel level) {
    return createMessage(msg, msgPars, null, level);
  }

  public static IBizMessage createMessage(String msg, String[] msgPars, MessageLocation location,
      MessageLevel level) {
    BizMessage tempVar = new BizMessage();
    tempVar.setMessageFormat(msg);
    tempVar.setMessageParams(msgPars);
    tempVar.setLocation(location);
    tempVar.setLevel(level);
    BizMessage rez = tempVar;
    return rez;
  }

  public final MessageLevel getMaxLevel() {
    if (messages == null || messages.isEmpty()) {
      return MessageLevel.Success;
    }

    return Collections.max(messages, (msg1, msg2) -> msg1.getLevel().compareTo(msg2.getLevel()))
        .getLevel();
  }

  @Override
  public final void addMessage(IBizMessage msg) {
    messages.add(msg);
  }

  @Override
  public final java.util.List<IBizMessage> getMessages() {
    return Collections.unmodifiableList(messages);
  }

  public final void clear() {
    messages.clear();
  }
}
