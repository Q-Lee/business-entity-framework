/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.utils;

public final class TenantUtil
{
	public static String GetTenantColumnName()
	{
		throw new UnsupportedOperationException();
		//Java版临时注释
//		//此结果不带表别名
//		return ServiceManager.<ITenantUtils>GetService().TenantField;
	}

//	public static IDbDataParameter GetTenancyDataParam(String aliasName)
//	{
//		return ServiceManager.<ITenantUtils>GetService().GetTenancyDataParam(aliasName);
//	}

//	public static String GetCondition(String aliasName)
//	{
//		return ServiceManager.<ITenantUtils>GetService().GetTenancyFilterSQL(aliasName);
//	}

	public static boolean IsDiscriminator()
	{
		//var tenantId = CafContext.Current.TenantId;

		////TODO 等caf提供新的接口封装
		////var info = ServiceManager.GetService<ITenantService>().GetDBConnInfo(tenantId, CafContext.Current.AppCode, CafContext.Current.CurrentSU);
		//string appCode = CafContext.Current.AppCode;
		//var info = ServiceManager.GetService<ITenantService>().GetAppInstanceInfo(tenantId, appCode);

		//if (info.DataIsolation == DataIsolation.Discriminator)
		//    return true;
		return false;
	}
}
