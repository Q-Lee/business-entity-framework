/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.utils;

import com.inspur.edp.caf.db.dbaccess.IConverObjectToListFunc;
import com.inspur.edp.cef.api.action.QueryResult;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.repository.adaptoritem.EntityRelationalReposAdaptor;
import com.inspur.edp.cef.repository.adaptoritem.QueryContext;

import java.util.List;

public final class PaginationConvertor implements IConverObjectToListFunc<IEntityData> {
    private EntityRelationalReposAdaptor adaptor;
    private QueryContext queryContext;
    private QueryResult queryResult;

    public PaginationConvertor(EntityRelationalReposAdaptor adaptor, QueryContext queryContext)
    {
        this.adaptor = adaptor;
        this.queryContext = queryContext;
    }

    @Override
    public final List<IEntityData> convert(List<Object[]> list) {
        queryResult =  null;//adaptor.innerGetDatas(list,null,queryContext);
        return queryResult.getQueryResult();
    }

    public final QueryResult getQueryResult() {
        return queryResult;
    }
}
