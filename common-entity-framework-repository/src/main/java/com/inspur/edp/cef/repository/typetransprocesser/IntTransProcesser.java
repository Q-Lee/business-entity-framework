/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.typetransprocesser;

import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.entity.condition.FilterCondition;

import java.sql.Connection;

public class IntTransProcesser implements ITypeTransProcesser
{
	private static IntTransProcesser instance;
	public static IntTransProcesser getInstacne()
	{
		if (instance == null)
		{
			instance = new IntTransProcesser();
		}
		return instance;
	}
	private IntTransProcesser()
	{

	}
	public final Object transType(FilterCondition filter, Connection db)
	{
		return Integer.parseInt(filter.getValue());
	}

	public final Object transType(Object value)
	{
		return this.transType(value,true);
	}

	@Override
	public Object transType(Object value, boolean isNull) {
		if(value == null){
			if(!isNull){
				return 0;
			}
			return null;
		}
		if (value instanceof Integer || value.getClass() == int.class)
		{
			return value;
		}
		try
		{
			int result = (Integer)value;
			return result;
		}
		catch (RuntimeException e)
		{
			throw new RuntimeException("ErrorType", e);
		}
	}
}
