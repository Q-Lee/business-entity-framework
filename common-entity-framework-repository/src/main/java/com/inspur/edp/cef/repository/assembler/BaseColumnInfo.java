/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler;

public abstract class BaseColumnInfo
{
	private String privateColumnName;
	public final String getColumnName()
	{
		return privateColumnName;
	}
	public final void setColumnName(String value)
	{
		privateColumnName = value;
	}
	private boolean privateIsMultiLang;
	public final boolean getIsMultiLang()
	{
		return privateIsMultiLang;
	}
	public final void setIsMultiLang(boolean value)
	{
		privateIsMultiLang = value;
	}
	private boolean privateIsUdtElement;
	public final boolean getIsUdtElement()
	{
		return privateIsUdtElement;
	}
	public final void setIsUdtElement(boolean value)
	{
		privateIsUdtElement = value;
	}
	private boolean privateIsAssociation;
	public final boolean getIsAssociation()
	{
		return privateIsAssociation;
	}
	public final void setIsAssociation(boolean value)
	{
		privateIsAssociation = value;
	}
	private boolean privateIsEnum;
	public final boolean getIsEnum()
	{
		return privateIsEnum;
	}
	public final void setIsEnum(boolean value)
	{
		privateIsEnum = value;
	}
	private boolean privateIsAssociateRefElement;
	public final boolean getIsAssociateRefElement()
	{
		return privateIsAssociateRefElement;
	}
	public final void setIsAssociateRefElement(boolean value)
	{
		privateIsAssociateRefElement = value;
	}
	private String privateBelongElementLabel;
	public final String getBelongElementLabel()
	{
		return privateBelongElementLabel;
	}
	public final void setBelongElementLabel(String value)
	{
		privateBelongElementLabel = value;
	}
}
