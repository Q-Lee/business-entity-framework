/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler.nesteddatatype;

import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.api.repository.INestedPersistenceValueReader;
import com.inspur.edp.cef.repository.assembler.BaseColumnInfo;

public class NestedColumnInfo extends BaseColumnInfo
{
	public NestedColumnInfo()
	{
	}

	private GspDbDataType privateColumnType;
	public final GspDbDataType getColumnType()
	{
		return privateColumnType;
	}
	public final void setColumnType(GspDbDataType value)
	{
		privateColumnType = value;
	}
	private INestedPersistenceValueReader privatePersistenceValueReader;
	public final INestedPersistenceValueReader getPersistenceValueReader()
	{
		return privatePersistenceValueReader;
	}
	public final void setPersistenceValueReader(INestedPersistenceValueReader value)
	{
		privatePersistenceValueReader = value;
	}
}
