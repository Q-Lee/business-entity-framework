/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.utils;

import com.inspur.edp.caf.db.dbaccess.*;
import com.inspur.edp.cef.api.RefObject;
import com.inspur.edp.cef.entity.dependenceTemp.Pagination;
import com.inspur.edp.cef.entity.entity.IEntityData;
import com.inspur.edp.cef.repository.adaptor.EntityRelationalAdaptor;

import com.inspur.edp.cef.repository.adaptoritem.EntityRelationalReposAdaptor;

import java.util.ArrayList;
import java.util.List;

public final class DatabaseUtil
{
	public static ArrayList<IEntityData> getPaginationData(
			String execTableName,
			String tableName,
			String execColumnNames,
			String pkField,
			String execFilterCondition,
			String execSortCondition,
			ArrayList<com.inspur.edp.cef.api.repository.DbParameter> parameters,
			RefObject<Pagination> pagination,
			EntityRelationalAdaptor adaptor, boolean isView)
	{
		int pageSize = pagination.argvalue.getPageSize();
		int pageNumber = pagination.argvalue.getPageIndex();
		RowCount rowCount = new RowCount();
		RevisedPageNumber revisedpageNumber = new RevisedPageNumber();
		Database database = new Database();
		List<IEntityData> queryResult = null;
		if(isView){
			queryResult = database.<IEntityData>getPaginaryDataForView(
					execTableName,
					execColumnNames,
					pkField,
					execFilterCondition,
					execSortCondition,
					"",
					tableName,
					pageSize,
					pageNumber,
					"",
					rowCount,
					revisedpageNumber,
					buildBqlParameters(parameters),
					adaptor::getDatasInPagination);
		}
		else{
			queryResult = database.<IEntityData>getPaginaryData(
					execTableName,
					execColumnNames,
					pkField,
					execFilterCondition,
					execSortCondition,
					"",
					tableName,
					pageSize,
					pageNumber,
					"",
					rowCount,
					revisedpageNumber,
					buildBqlParameters(parameters),
					adaptor::getDatasInPagination);
		}


		pagination.argvalue.setTotalCount(rowCount.number);
		pagination.argvalue.setPageIndex(revisedpageNumber.number);
//		pagination.argvalue.setPageCount(rowCount.number / pageSize + rowCount.number % pageSize);
		return (ArrayList)queryResult;
	}

	public static ArrayList<IEntityData> getPaginationData(
			String execTableName,
			String tableName,
			String execColumnNames,
			String pkField,
			String execFilterCondition,
			String execSortCondition,
			ArrayList<com.inspur.edp.cef.api.repository.DbParameter> parameters,
			RefObject<Pagination> pagination,
			EntityRelationalReposAdaptor adaptor, boolean isView)
	{
		int pageSize = pagination.argvalue.getPageSize();
		int pageNumber = pagination.argvalue.getPageIndex();
		RowCount rowCount = new RowCount();
		RevisedPageNumber revisedpageNumber = new RevisedPageNumber();
		Database database = new Database();
		List<IEntityData> queryResult = null;
		if(isView){
			queryResult = database.<IEntityData>getPaginaryDataForView(
					execTableName,
					execColumnNames,
					pkField,
					execFilterCondition,
					execSortCondition,
					"",
					tableName,
					pageSize,
					pageNumber,
					"",
					rowCount,
					revisedpageNumber,
					buildBqlParameters(parameters),
					adaptor::getDatasInPagination);
		}
		else{
			queryResult = database.<IEntityData>getPaginaryData(
					execTableName,
					execColumnNames,
					pkField,
					execFilterCondition,
					execSortCondition,
					"",
					tableName,
					pageSize,
					pageNumber,
					"",
					rowCount,
					revisedpageNumber,
					buildBqlParameters(parameters),
					adaptor::getDatasInPagination);
		}


		pagination.argvalue.setTotalCount(rowCount.number);
		pagination.argvalue.setPageIndex(revisedpageNumber.number);
//		pagination.argvalue.setPageCount(rowCount.number / pageSize + rowCount.number % pageSize);
		return (ArrayList)queryResult;
	}

	private static IDbParameter[] buildBqlParameters(ArrayList<com.inspur.edp.cef.api.repository.DbParameter> parameters){
		IDbParameter[] params = new DbParameter[parameters.size()];
		for (int i = 0;i<parameters.size();i++){
			com.inspur.edp.cef.api.repository.DbParameter sourceParam = parameters.get(i);
			DbParameter targetParam = new DbParameter();
			targetParam.setDataType(transDbType(sourceParam.getDataType()));
			targetParam.setParameterName(null);
			targetParam.setIndex(i);
			targetParam.setParameterValue(sourceParam.getValue());
			params[i] = targetParam;
		}

		return params;
	}

	private static GspDbDataType transDbType(com.inspur.edp.cef.api.repository.GspDbDataType dbType){
		switch (dbType){
			case Char:
				return GspDbDataType.Char;
			case NChar:
				return GspDbDataType.NChar;
			case VarChar:
				return GspDbDataType.VarChar;
			case NVarChar:
				return GspDbDataType.NVarChar;
			case Int:
				return GspDbDataType.Int;
			case Decimal:
				return GspDbDataType.Decimal;
			case DateTime:
			case Date:
				return GspDbDataType.DateTime;
			case Blob:
				return GspDbDataType.Blob;
			case Clob:
				return GspDbDataType.Clob;
			case NClob:
				return GspDbDataType.NClob;
			default:
				return GspDbDataType.UnKnown;
		}
	}
	public  static String getColumnName(String filedName){
		if(filedName.contains(".")){
			String columnName[]=filedName.split("[.]");
			int length=columnName.length-1;
			return columnName[length];
		}
		return filedName;
	}

}
