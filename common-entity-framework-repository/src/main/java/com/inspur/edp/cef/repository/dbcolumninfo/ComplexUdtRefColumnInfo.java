/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.dbcolumninfo;

import com.inspur.edp.cef.api.repository.GspDbDataType;
import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.spi.entity.info.propertyinfo.DataTypePropertyInfo;

public class ComplexUdtRefColumnInfo extends DbColumnInfo {
  ComplexUdtRefColumnInfo(String columnName,String dbColumnName, GspDbDataType columnType,int length,int precision,boolean isPrimaryKey,boolean isAssociateRefElement,boolean isMultiLang,boolean isParentId,boolean isUdtElement
      ,boolean isAssociation,boolean isEnum,String belongElementLabel,
      ITypeTransProcesser typeTransProcesser)
  {
    super(columnName,dbColumnName,columnType,length,precision,isPrimaryKey,isAssociateRefElement,isMultiLang,isParentId,isUdtElement,isAssociation,isEnum,belongElementLabel,typeTransProcesser);
  }

  @Override
  public boolean isUdtRefColumn() {
    return true;
  }

  private DataTypePropertyInfo belongUdtPropertyInfo;

  public DataTypePropertyInfo getBelongUdtPropertyInfo() {
    return belongUdtPropertyInfo;
  }

  public void setBelongUdtPropertyInfo(
      DataTypePropertyInfo belongUdtPropertyInfo) {
    this.belongUdtPropertyInfo = belongUdtPropertyInfo;
  }
}
