/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.utils;


import com.inspur.edp.cef.entity.dependenceTemp.DataValidator;
import com.inspur.edp.cef.entity.i18n.MultiLanguageInfo;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.i18n.framework.api.language.EcpLanguage;
import io.iec.edp.caf.i18n.framework.api.language.ILanguageService;
import io.iec.edp.caf.runtime.config.CefBeanUtil;
import lombok.var;

import java.util.List;

public final class RepositoryUtil {
	//public static string FormatFiscal(string sql, string dboID)
	//{
	//    DataValidator.checkForEmptyString(sql, "sql");
	//  //  CafContext.Current.Session.Set("FIYear","2021");
	//    string tableName = ServiceManager.GetService<IDatabaseObjectRtService>().GetTableNameWithSession(dboID);      

	//    string[] str = sql.Split(' ');
	//    for (int i=0;i<str.Length;i++)
	//    {
	//        if (str[i].contains("@YY@"))
	//            str[i] = tableName;
	//    }
	//    StringBuilder sb = new StringBuilder();
	//    foreach (var item in str)
	//    {
	//        sb.Append(item);
	//        sb.Append(' ');      
	//    }         
	//    return sb.ToString();
	//}

	public static String FormatMuliLang(String sql) {
		DataValidator.checkForEmptyString(sql, "sql");

//`
//		var fieldSuffix = ServiceManager.<ILanguageService>GetService().GetFieldSuffix(CafContext.Current.Language);
		String fieldSuffix = CefBeanUtil.getAppCtx().getBean(ILanguageService.class).getFieldSuffix(CAFContext.current.getLanguage());
//		String fieldSuffix="_chs";
		return sql.replaceAll("@Language@", fieldSuffix);
	}

	public static String FormatMuliLang(String sql, String fieldSuffix) {
		DataValidator.checkForEmptyString(sql, "sql");
		return sql.replaceAll("@Language@", fieldSuffix);
	}


	public static List<EcpLanguage> getCurrentEnabledLanguages() {
		return CefBeanUtil.getAppCtx().getBean(ILanguageService.class).getEnabledLanguages();
	}

	public static EcpLanguage getLanguage(String languageCode) {
		return CefBeanUtil.getAppCtx().getBean(ILanguageService.class).getLanguage(languageCode);
	}


	public static String FormatMuliLangColumnName(String columnName, EcpLanguage language) {
		DataValidator.checkForEmptyString(columnName, "columnName");
		String fieldSuffix =
				CefBeanUtil.getAppCtx().getBean(ILanguageService.class).getFieldSuffix(language.getId());
		// _CHS
		if(columnName.contains("@Language@")){
			return columnName.replaceAll("@Language@", fieldSuffix);
		} else if (columnName.contains(MultiLanguageInfo.MULTILANGUAGETOKEN)) {
			return columnName.replaceAll(MultiLanguageInfo.MULTILANGUAGETOKEN, fieldSuffix);
		} else {
			return columnName.concat(fieldSuffix);
		}
	}
}
