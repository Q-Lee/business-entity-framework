/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.adaptor;

import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;

import java.util.ArrayList;
import java.util.HashMap;

public class KeyWordsManager {
    private static HashMap<String, ArrayList<String>> keyWordsMap = new HashMap<>();
    static {
        ArrayList<String> mySqlList = new ArrayList<>();
        mySqlList.add("range");
        mySqlList.add("condition");
        mySqlList.add("status");
        mySqlList.add("separator");
        keyWordsMap.put(DbType.MySQL.toString(), mySqlList);

        ArrayList<String> oscarList = new ArrayList<>();
        oscarList.add("position");;
        keyWordsMap.put(DbType.Oscar.toString(), oscarList);

        ArrayList<String> sqlserverList = new ArrayList<>();
        sqlserverList.add("identity");;
        keyWordsMap.put(DbType.SQLServer.toString(), sqlserverList);

        ArrayList<String> dmList = new ArrayList<>();
        dmList.add("identity");;
        keyWordsMap.put(DbType.DM.toString(), dmList);
    }

    public static ArrayList<String> getDBKeyWords(DbType dbType){
        return keyWordsMap.get(dbType.toString());
    }

    public static boolean containKeyWord(String keyWord){
        if(keyWordsMap.containsKey(CAFContext.current.getDbType().toString())){
            ArrayList<String> list = getDBKeyWords(CAFContext.current.getDbType());
            if(list !=null && list.contains(keyWord.toLowerCase())){
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    //处理节点别名是关键字 只处理神通
    public static String getTableAlias(String tableAlias){
        tableAlias = tableAlias.trim();
        tableAlias = tableAlias.replace("$", "");
        if(containKeyWord(tableAlias)){
            switch (CAFContext.current.getDbType()){
                case Oscar:
                    tableAlias = getOscarTableAlias(tableAlias);
                    break;
                case SQLServer:
                    tableAlias = getSQLServerTableAlias(tableAlias);
                    break;
                case DM:
                    tableAlias = getDMTableAlias(tableAlias);
                    break;
            }
        }
        return tableAlias;
    }

    /**
     *
     * @param tableAlias
     * @param ignoreKeyWords
     * @return
     */
    public static String getTableAlias(String tableAlias, boolean ignoreKeyWords){
        if(ignoreKeyWords){
            tableAlias = tableAlias.trim();
            tableAlias = tableAlias.replace("$", "");
            return tableAlias;
        }
        else {
            return getTableAlias(tableAlias);
        }
    }

    //处理字段是关键字 只处理MySQL
    public static String getColumnAlias(String propName){
        if(containKeyWord(propName)){
            switch (CAFContext.current.getDbType()){
                case MySQL:
                    propName = getMySQLColumnAlias(propName);
                    break;
                case SQLServer:
                    propName = getSQLServerColumnAlias(propName);
                    break;
                case DM:
                    propName = getDMColumnAlias(propName);
                    break;
            }
        }
        return propName;
    }

    public static String getMySQLColumnAlias(String keyWord){
        return  "`" + keyWord + "`";
    }

    public static String getSQLServerColumnAlias(String keyWord){
        return  "[" + keyWord + "]";
    }

    public static String getDMColumnAlias(String keyWord){
        return  keyWord + "alias";
    }

    public static String getDMTableAlias(String keyWord){
        return  "\"" + keyWord + "\"";
    }

    public static String getOscarTableAlias(String keyWord){
        return  "\"" + keyWord + "\"";
    }

    public static String getSQLServerTableAlias(String keyWord){
        return  "[" + keyWord + "]";
    }
}
