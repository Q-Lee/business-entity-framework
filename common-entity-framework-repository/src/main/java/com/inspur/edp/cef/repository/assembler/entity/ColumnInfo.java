/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.assembler.entity;


import com.inspur.edp.cef.api.repository.ITypeTransProcesser;
import com.inspur.edp.cef.repository.assembler.BaseColumnInfo;

public class ColumnInfo extends BaseColumnInfo implements Cloneable
{

	private String privateDboColumnId;
	public final String getDboColumnId()
	{
		return privateDboColumnId;
	}
	public final void setDboColumnId(String value)
	{
		privateDboColumnId = value;
	}
	private boolean privateIsPrimaryKey;
	public final boolean getIsPrimaryKey()
	{
		return privateIsPrimaryKey;
	}
	public final void setIsPrimaryKey(boolean value)
	{
		privateIsPrimaryKey = value;
	}
	private boolean privateIsParentId;
	public final boolean getIsParentId()
	{
		return privateIsParentId;
	}
	public final void setIsParentId(boolean value)
	{
		privateIsParentId = value;
	}
	private ITypeTransProcesser privateTypeTransProcesser;
	public final ITypeTransProcesser getTypeTransProcesser()
	{
		return privateTypeTransProcesser;
	}
	public final void setTypeTransProcesser(ITypeTransProcesser value)
	{
		privateTypeTransProcesser = value;
	}
	//public Func<FilterCondition, IGSPDatabase, object> typetransprocesser { get; set; }
	public final Object clone() throws CloneNotSupportedException {
		ColumnInfo info =(ColumnInfo)(super.clone());
		return  info;
	}
}
