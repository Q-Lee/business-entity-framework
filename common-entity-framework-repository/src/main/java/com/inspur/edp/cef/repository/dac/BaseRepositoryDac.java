/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.dac;

import com.inspur.edp.cef.api.repository.GspDbType;
import com.inspur.edp.cef.api.repository.adaptor.IEntityAdaptor;
import com.inspur.edp.cef.api.repository.adaptor.IRepositoryAdaptor;
import com.inspur.edp.cef.api.repository.dac.IDataTypeDac;
import com.inspur.edp.cef.entity.entity.ICefData;
import com.inspur.edp.cef.repository.adaptor.BaseEntityAdaptor;
import com.inspur.edp.cef.repository.assembler.AbstractDataAdapterExtendInfo;
import io.iec.edp.caf.boot.context.CAFContext;
import io.iec.edp.caf.commons.dataaccess.DbType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public abstract class BaseRepositoryDac implements IDataTypeDac {
    protected Map<String, Object> pars;
    private HashMap<String, String> vars;
    protected ArrayList<AbstractDataAdapterExtendInfo> extendInfos = new ArrayList<>();

    protected  int getVersion(){return 0;}


    private static GspDbType transGspDbType(DbType dbType,int version) {
        if (dbType == null)
            throw new RuntimeException("获取当前数据库类型失败");
        GspDbType gspDbType;
        switch (dbType) {
            case Oracle:
                gspDbType = GspDbType.Oracle;
                break;
            case SQLServer:
                gspDbType = GspDbType.SQLServer;
                break;
            case DM:
                gspDbType = GspDbType.DM;
                break;
            case PgSQL:
            case HighGo:
                gspDbType = GspDbType.PgSQL;
                break;
            case Gbase:
                gspDbType = GspDbType.GBase;
                break;
            case MySQL:
                if(version==0)
                    gspDbType=GspDbType.SQLServer;
                else
                    gspDbType = GspDbType.MySQL;
                break;
            case Oscar:
                if(version==0)
                    gspDbType= GspDbType.Oracle;
                else
                    gspDbType = GspDbType.Oscar;
                break;
            case Kingbase:
                if(version==0)
                    gspDbType= GspDbType.Oracle;
                else
                    gspDbType = GspDbType.Kingbase;
                break;
             case DB2:
                    gspDbType= GspDbType.DB2;
                break;
            default:
                gspDbType = GspDbType.Unknown;
                break;
        }
        return gspDbType;
    }

    public ArrayList<AbstractDataAdapterExtendInfo> getExtendInfo() {
        return extendInfos;
    }

    public void setExtendInfo(ArrayList<AbstractDataAdapterExtendInfo> extendInfos) {
        this.extendInfos = extendInfos;
    }

    public Map<String, Object> getPars() {
        return this.pars;
    }

    public HashMap<String, String> getVars() {
        return vars;
    }

    @Override
    public void initParams(Map<String, Object> pars) {
        this.pars = pars;
    }

    public final void initRepoVariables(HashMap<String, String> vars) {
        this.vars = vars;
    }

    protected abstract IEntityAdaptor innerGetAdaptor();

    @Override
    public Object getPersistenceValue(String colName, ICefData data) {
        return innerGetAdaptor().getPersistenceValue(colName, data);
    }

    @Override
    public final IRepositoryAdaptor getAdaptor() {
        IEntityAdaptor adaptor = innerGetAdaptor();
        adaptor.initParams(pars);
        adaptor.initVars(vars);
        if(extendInfos != null && adaptor instanceof BaseEntityAdaptor)
            ((BaseEntityAdaptor)adaptor).setDataAdapterExtendInfos(extendInfos);
        return adaptor;
    }

    protected final GspDbType getDbType() {
        return transGspDbType(CAFContext.current.getDbType(),getVersion());
    }

}
