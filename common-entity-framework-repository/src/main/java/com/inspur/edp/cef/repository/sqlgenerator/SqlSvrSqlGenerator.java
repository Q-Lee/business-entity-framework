/*
 *    Copyright © OpenAtom Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.inspur.edp.cef.repository.sqlgenerator;

public class SqlSvrSqlGenerator extends BaseSqlGenerator
{
	@Override
	public String getQuerySql(){return "SELECT %1$s FROM %2$s ";}
	@Override
	public String getInsertSql(){return "INSERT INTO %1$s (%2$s) VALUES(%3$s) ";}
	@Override
	public String getDeleteSql(){return "DELETE %1$s FROM %1$s %2$s "; }//"WHERE %3$s = %4$s";
	@Override
	public String getUpdateSql(){return  "UPDATE %1$s SET %2$s FROM %1$s %3$s WHERE %4$s = %5$s";}
	@Override
	public String getSearchSqlByRootIds (){ return "SELECT %1$s, %2$s " + "FROM %3$s " + "%4$s " + "WHERE %2$s in%5$s ";}
	@Override
	public String getSearchSqlByRootId(){return  "SELECT %1$s, %2$s " + "FROM %3$s " + "%4$s " + "WHERE %2$s = %5$s ";}
	@Override
	public String getDeleteByRoot(){return "DELETE %1$s FROM %1$s %2$s WHERE EXISTS (SELECT 1 FROM %3$s WHERE %4$s AND %5$s= %6$s)";}
	@Override
	public String getRetrieveBatchSql(){return  "SELECT %1$s FROM %2$s WHERE %3$s IN%4$s";}
	@Override
	public String getRetrieveSql(){return "SELECT %1$s FROM %2$s WHERE %3$s = %4$s";}
	@Override
	public String getJoinTableName(){return " LEFT OUTER JOIN %1$s ON %2$s = %3$s ";}
	@Override
	public String getInnerJoinTableName(){ return "JOIN @ParentTableName@ @ParentTableAlias@ ON @ParentID@ = @PrimaryID@";}
}
